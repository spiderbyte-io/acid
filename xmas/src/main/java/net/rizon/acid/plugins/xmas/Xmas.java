package net.rizon.acid.plugins.xmas;

import com.google.common.eventbus.Subscribe;
import io.netty.util.concurrent.ScheduledFuture;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.User;
import net.rizon.acid.events.EventChanMode;
import net.rizon.acid.events.EventJoin;
import net.rizon.acid.plugins.Plugin;
import net.rizon.acid.plugins.xmas.conf.Config;
import net.rizon.acid.plugins.xmas.conf.VhostDay;
import net.rizon.acid.plugins.xmas.events.Vhost;
import net.rizon.acid.plugins.xmas.events.Voice;

public class Xmas extends Plugin
{
	private static Config conf;
	private static ScheduledFuture voiceTimer;
	private static ScheduledFuture topicTimer;

	public static String getXmasChanName()
	{
		return Acidictive.conf.getChannelNamed(conf.xmaschan);
	}
	
	private static ZoneId getTimezone()
	{
		return ZoneId.of(conf.timezone);
	}

	@Override
	public void start() throws Exception
	{
		Acidictive.eventBus.register(this);
		
		reload();
	}

	@Override
	public void stop()
	{
		Acidictive.eventBus.unregister(this);

		this.stopTimers();
	}

	@Subscribe
	public void onJoin(EventJoin event)
	{
		final Channel channel = event.getChannel();
		final User[] users = event.getUsers();

		if (!channel.getName().equalsIgnoreCase(getXmasChanName()) || users.length > 1)
			return;
		(new Vhost(users[0], false)).run();
	}

	@Subscribe
	public void onChanMode(EventChanMode event)
	{
		final Channel channel = event.getChan();
		if (!channel.getName().equalsIgnoreCase(getXmasChanName()))
			return;

		// todo Implement mode parsing in EventChanMode
		final String mode = event.getModes();
		final String[] split = mode.split(" ");
		if (split.length != 2 || !split[0].equals("+v"))
			return;

		final User voicedUser = User.findUser(split[1]);

		// You're not real.
		if (voicedUser == null)
			return;

		(new Vhost(voicedUser, true)).run();
	}

	@Override
	public void reload() throws Exception
	{
		conf = (Config) net.rizon.acid.conf.Config.load("xmas.yml", Config.class);
		this.stopTimers();

		voiceTimer = Acidictive.scheduleWithFixedDelay(new Voice(), conf.specialminutes, TimeUnit.MINUTES);
		this.initTopicTimer();
		this.updateTopic();
	}

	/**
	 * Calculate the time until midnight and start a timer with it that will
	 * start a timer that repeats every day to set the topic. Yeah.
	 */
	private void initTopicTimer()
	{
		final LocalDateTime tomorrowMidnight = LocalDateTime.of(LocalDate.now(Xmas.getTimezone()), LocalTime.MIDNIGHT).plusDays(1);
		final long untilMidnight = tomorrowMidnight.toEpochSecond(ZoneOffset.UTC) - LocalDateTime.now(Xmas.getTimezone()).toEpochSecond(ZoneOffset.UTC);
		topicTimer = Acidictive.schedule(() ->
		{
			this.updateTopic();
			topicTimer = Acidictive.scheduleAtFixedRate(this::updateTopic, 24, TimeUnit.HOURS);
		}, untilMidnight, TimeUnit.SECONDS);
	}

	/**
	 * Stop running timers if any
	 */
	private void stopTimers()
	{
		if (voiceTimer != null)
			voiceTimer.cancel(true);
		if (topicTimer != null)
			topicTimer.cancel(true);
	}

	private void updateTopic()
	{
		final String vhost = Xmas.todaysVhost();
		if (vhost == null)
			return;

		if (User.findUser("ChanServ") != null)
		{
			Acidictive.privmsg("ChanServ", String.format("TOPIC %s %s", Xmas.getXmasChanName(), String.format(conf.topic, vhost)));
		}
	}

	/**
	 * Return today's vhost, or {@code null} if we're not being festive
	 *
	 * @return String
	 */
	public static String todaysVhost()
	{
		final LocalDateTime now = LocalDateTime.now(Xmas.getTimezone());
		final String today = now.format(DateTimeFormatter.ofPattern("dd-MM"));
		Optional<VhostDay> has = conf.vhosts.stream().filter((v) -> v.getDate().equals(today)).findFirst();
		if (has.isPresent() == false)
			return null;
		return has.get().getVhost();
	}
}