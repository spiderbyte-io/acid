/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.rizon.acid.arguments;

import java.time.Duration;
import java.util.Arrays;
import java.util.Collection;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

/**
 * Runs a parameterized set of tests of correct values.
 *
 * @author orillion <orillion@rizon.net>
 */
@RunWith(Parameterized.class)
public class ExpiryArgumentSuccessTest
{
	private static final long SECOND = 1;
	private static final long MINUTE = 60 * SECOND;
	private static final long HOUR = 60 * MINUTE;
	private static final long DAY = 24 * HOUR;
	private static final long WEEK = 7 * DAY;
	private static final long YEAR = 365 * DAY;

	@Parameters
	public static Collection<Object[]> data()
	{
		return Arrays.asList(new Object[][]
		{
			{
				"+30d", Duration.ofDays(30)
			},
			{
				"+3w", Duration.ofSeconds(3 * WEEK)
			},
			{
				"+42s", Duration.ofSeconds(42 * SECOND)
			},
			{
				"+2y", Duration.ofSeconds(2 * YEAR)
			},
			{
				"+5h", Duration.ofHours(5)
			},
			{
				"+90m", Duration.ofMinutes(90)
			},
		});
	}

	@Parameter(value = 0)
	public String argument;
	@Parameter(value = 1)
	public Duration duration;

	@Test
	public void test()
	{
		ExpiryArgument arg = ExpiryArgument.parse(argument);

		Assert.assertEquals(duration, arg.getDuration());
		Assert.assertEquals(argument, arg.getArgument());
	}
}
