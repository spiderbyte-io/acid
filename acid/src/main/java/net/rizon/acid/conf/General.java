package net.rizon.acid.conf;

import java.util.List;

public class General implements Validatable
{
	public List<String> ulines;
	public String command_prefix, email_from, email_smtp, email_securityemail;
	public String control;
	public String akillserv;

	@Override
	public void validate() throws ConfigException
	{
		Validator.validateNotEmpty("command_prefix", command_prefix);
		Validator.validateNotEmpty("email_from", email_from);
		Validator.validateNotEmpty("email_smtp", email_smtp);
		Validator.validateNotEmpty("email_securiyemail", email_securityemail);
		Validator.validateNotEmpty("control", control);
	}
}