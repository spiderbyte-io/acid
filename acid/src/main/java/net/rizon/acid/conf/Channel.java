package net.rizon.acid.conf;

public class Channel implements Validatable
{
	public String name, channel;
	public String blowfish;

	@Override
	public void validate() throws ConfigException
	{
		Validator.validateNotEmpty("name", name);
		Validator.validateNotEmpty("channel", channel);
	}
}