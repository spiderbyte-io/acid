package net.rizon.acid.core;

import net.rizon.acid.plugins.Plugin;
import com.google.common.eventbus.EventBus;
import io.netty.util.concurrent.ScheduledFuture;
import java.net.MalformedURLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import net.rizon.acid.capab.QuitStorm;
import net.rizon.acid.conf.Client;
import net.rizon.acid.conf.Config;
import net.rizon.acid.conf.PluginDesc;
import net.rizon.acid.events.EventCTCP;
import net.rizon.acid.events.EventCTCPReply;
import net.rizon.acid.events.EventChanMode;
import net.rizon.acid.events.EventCommandCertFPMismatch;
import net.rizon.acid.events.EventEOB;
import net.rizon.acid.events.EventEncap;
import net.rizon.acid.events.EventInvite;
import net.rizon.acid.events.EventJoin;
import net.rizon.acid.events.EventKick;
import net.rizon.acid.events.EventKill;
import net.rizon.acid.events.EventNickChange;
import net.rizon.acid.events.EventNotice;
import net.rizon.acid.events.EventPart;
import net.rizon.acid.events.EventPrivmsg;
import net.rizon.acid.events.EventQuit;
import net.rizon.acid.events.EventServerLink;
import net.rizon.acid.events.EventServerNotice;
import net.rizon.acid.events.EventSync;
import net.rizon.acid.events.EventUserConnect;
import net.rizon.acid.events.EventWebIRC;
import net.rizon.acid.events.ExceptionHandler;
import net.rizon.acid.logging.LoggerUtils;
import net.rizon.acid.plugins.PluginManager;
import net.rizon.acid.sql.SQL;
import net.rizon.acid.util.Blowfish;
import net.rizon.acid.plugins.ClassLoader;
import net.rizon.acid.util.CloakGenerator;
import net.rizon.acid.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Acidictive extends AcidCore
{
	private static final Logger log = LoggerFactory.getLogger(Acidictive.class);
	// logs here don't go to irc
	public static final Logger fileLogger = LoggerFactory.getLogger("file");

	public static CloakGenerator cloakGenerator;
	public static Config conf;
	public static long startTime;
	public static int syncTime;
	public static SQL acidcore_sql;
	public static char bold = '\u0002';
	
	public static ClassLoader loader; // loader for the core commands

	public static EventBus eventBus = new EventBus(new ExceptionHandler());
	private static QuitStorm quitStorm;

	public static String getVersion()
	{
		return "acid-" + Version.PROJECT_VERSION + "-" + Version.GIT_REVISION_SHORT;
	}

	public static void main(String[] args)
	{
		LoggerUtils.initUncaughtExceptionHandler();
		
		log.info(getVersion() + " starting up...");

		try
		{
			conf = (Config) Config.load("acidictive.yml", Config.class);
		}
		catch (Exception ex)
		{
			log.error("Unable to start due to configuration problems", ex);
			System.exit(1);
		}
		
		try
		{
			cloakGenerator = new CloakGenerator(conf.serverinfo.cloakkeys, conf.serverinfo.network + "-");
		}
		catch (IllegalArgumentException ex)
		{
			log.error(null, ex);
			System.exit(1);
		}

		quitStorm = new QuitStorm(eventBus, User.getUsersC());

		AcidCore.start(conf.uplink.host, conf.uplink.port, conf.serverinfo.name, conf.serverinfo.description, conf.uplink.pass, conf.serverinfo.id, conf.uplink.ssl);

		acidcore_sql = SQL.getConnection("acidcore");
		if (acidcore_sql == null)
		{
			log.error("Unable to get connection for `acidcore`");
			System.exit(-1);
		}

		try
		{
			loader = new ClassLoader("net.rizon.acid.commands.");
		}
		catch (MalformedURLException ex)
		{
			log.error(null, ex);
			System.exit(-1);
		}
		
		if (conf.clients != null)
			for (Client c : conf.clients)
				new AcidUser(null, c);

		if (conf.plugins != null)
			for (PluginDesc desc : conf.plugins)
			{
				try
				{
					Plugin p = PluginManager.loadPlugin(desc.getGroupId(), desc.getArtifactId(), desc.getVersion());
					if (p == null)
					{
						log.error("Unable to load plugin {}", desc);
						System.exit(-1);
					}
					
					log.info("Loaded plugin " + p.getName());
				}
				catch (Exception ex)
				{
					log.error("Unable to load plugin " + desc, ex);
					System.exit(-1);
				}
			}

		try
		{
			AcidCore.run();
		}
		catch (InterruptedException ex)
		{
			log.error(null, ex);
		}

		acidcore_sql.shutdown();

		System.exit(0);
	}

	public static void onStart()
	{
		startTime = new Date().getTime();

		for (User u : User.getUsersC())
		{
			if (!(u instanceof AcidUser))
				continue;

			((AcidUser) u).introduce();
		}

		Protocol.eob();
	}

	public static void onNick(User user)
	{
		EventUserConnect event = new EventUserConnect();
		event.setUser(user);
		eventBus.post(event);
	}

	public static void onNickChange(User user, String oldnick)
	{
		EventNickChange event = new EventNickChange();
		event.setU(user);
		event.setOldNick(oldnick);
		eventBus.post(event);
	}

	public static void onEOB(Server server)
	{
		EventEOB event = new EventEOB();
		event.setServer(server);
		eventBus.post(event);
	}

	public static void onJoin(Channel channel, User[] users)
	{
		EventJoin event = new EventJoin();
		event.setChannel(channel);
		event.setUsers(users);
		eventBus.post(event);
	}

	public static void onPart(User user, Channel channel)
	{
		EventPart event = new EventPart();
		event.setChannel(channel);
		event.setUser(user);
		eventBus.post(event);
	}

	public static void onQuit(User user, String msg)
	{
		EventQuit event = new EventQuit();
		event.setUser(user);
		event.setMsg(msg);
		eventBus.post(event);
	}

	public static void onServer(Server server)
	{
		EventServerLink event = new EventServerLink();
		event.setServer(server);
		eventBus.post(event);
	}

	public static void onPrivmsg(String creator, String recipient, String msg)
	{
		try
		{
			User x = User.findUser(creator);

			EventPrivmsg event = new EventPrivmsg();
			event.setCreator(creator);
			event.setRecipient(recipient);
			event.setMsg(msg);
			eventBus.post(event);

			if (event.isStopProcessing())
				return;

			// All channel commands start with .
			if (recipient.startsWith("#") && !msg.startsWith(conf.general.command_prefix))
				return;

			if (x.getIdentNick().isEmpty())
			{
				// Only lookup flags for people who are +o and +r
				if (x.hasMode("o"))
				{
					// However +o and -r can do some commands (identify!)
					if (!x.getSU().isEmpty())
					{
						x.loadAccess(x);

						// Try to save a certfp if possible
						//if (!x.getFlags().isEmpty())
						//	Access.addCertFP(x, true);
					}
					// If +o-r, try to auth via cert; in case services are gone
					else if (!x.getCertFP().isEmpty())
					{
						// There might also be a CertFP for said user to use, check!
						try
						{
							PreparedStatement ps = Acidictive.acidcore_sql.prepare("SELECT `certfp` FROM `access` WHERE `user` = ?");
							ps.setString(1, x.getSU());
							ResultSet rs = Acidictive.acidcore_sql.executeQuery(ps);
							if (rs.next() && !rs.getString("certfp").isEmpty())
							{
								if (rs.getString("certfp").equalsIgnoreCase(x.getCertFP()))
								{
									x.loadAccess(x);
								}
								else
								{
									EventCommandCertFPMismatch e = new EventCommandCertFPMismatch();
									e.setU(x);
									e.setCertfp(rs.getString("certfp"));
									eventBus.post(event);
								}
							}
						}
						catch (SQLException ex)
						{
							log.warn("Unable to load certfp access", ex);
						}
					}
				}
			}

			// Not destined for anything else, must be a command.
			String[] parts = msg.split("\\s+");
			// strip leading "." if needed
			String command = (parts[0].startsWith(conf.general.command_prefix) ? parts[0].substring(conf.general.command_prefix.length()) : parts[0]).toLowerCase();
			String[] args = Arrays.copyOfRange(parts, 1, parts.length);

			Channel c = null;
			AcidUser to = null;
			net.rizon.acid.conf.Command confCommand = null;
			if (recipient.startsWith("#"))
			{
				c = Channel.findChannel(recipient);
				if (c == null)
					return;

				// lol?
				for (String s : c.getUsers())
				{
					User u = User.findUser(s);
					if (u instanceof AcidUser)
					{
						AcidUser t = (AcidUser) u;
						net.rizon.acid.conf.Command cmd = t.findConfCommand(command, c.getName());

						if (cmd != null)
						{
							to = t;
							confCommand = cmd;
						}
					}
				}

				if (to == null)
					return;
			}
			else
			{
				int i = recipient.indexOf('@');
				if (i != -1)
					recipient = recipient.substring(0, i);

				User targ = User.findUser(recipient);
				if (targ == null || !(targ instanceof AcidUser))
					return;

				to = (AcidUser) targ;
				confCommand = to.findConfCommand(command, null);
			}

			if (confCommand == null)
				return;

			// This isn't really necessary, but is faster than causing the core loader to fail and then fall back
			// by searching the plugin loaders.
			Plugin p = to.pkg;
			// Use our loader if there is no plugin
			ClassLoader cl = p != null ? p.loader : loader;
			Class<?> commandClass = cl.loadClass(confCommand.clazz);
			if (commandClass == null)
				return;

			if (recipient.startsWith("#"))
			{
				// In the proper channels commands can go without access checks. (ish)
				if (!confCommand.allowsChannel(recipient))
				{
					return; // Access
				}
				
				// Now that we're in the right channel check for just being logged in OR +o OR spoof
				if (!x.getIdentNick().isEmpty() || x.hasMode("o") || x.getIP().equals("0"))
				{
					// ok
				}
				else
				{
					// Only allow explicit "anyone" commands
					if (confCommand.privilege == null || !confCommand.privilege.equals("anyone"))
						return;
				}
			}
			// Message to me, check priv
			else
			{
				if (!x.hasMode("o"))
				{
					if (!"anyone".equals(confCommand.privilege))
					{
						// Only accept commands that are explicitly accessible
						// by anyone.
						return;
					}
				}
				else
				{
					// Non priv commands can be used by any oper
					if (confCommand.privilege == null)
					{
						log.info("Denied access to " + confCommand.name + " to " + x + " [command has no privilege]");
						// Commands with no priv can not be executed in PM
						return;
					}

					// Explicitly requires no privilege
					if (confCommand.privilege.equals("none") || "anyone".equals(confCommand.privilege))
						;
					else if (x.hasFlags(confCommand.privilege) == false)
					{
						log.info("Denied access to " + confCommand.name + " to " + x + " [user has no priv]");
						return;
					}
				}
			}
			
			Command cmd = (Command) commandClass.newInstance();

			if (args.length < cmd.GetMinArgs())
			{
				reply(x, to, c, "Syntax error for \2" +
						confCommand.name + "\2. Available commands are:");
				cmd.onHelp(x, to, c);
				return; // Syntax
			}

			if (args.length > cmd.GetMaxArgs() && cmd.GetMaxArgs() > 0)
			{
				String last = arrayFormat(args, cmd.GetMaxArgs() - 1, args.length - 1);
					String[] parsed_args = new String[cmd.GetMaxArgs()];
				for (int i = 0; i < cmd.GetMaxArgs() - 1; ++i)
					parsed_args[i] = args[i];
				parsed_args[cmd.GetMaxArgs() - 1] = last;
				args = parsed_args;
			}

			try
			{
				cmd.Run(x, to, c, args);

				/* log SUCCESSFUL privmsg command usages as well
				 * except help because that's just silly and nobody cares.
				 */
				if (!command.equalsIgnoreCase("help")
						&& !recipient.startsWith("#"))
					privmsg(conf.getChannelNamed("cmdlogchan"), x.getNick() + ": " + msg);
			}
			catch (Exception ex)
			{
				log.warn("Error running command " + confCommand.name, ex);
			}
		}
		catch (Exception e)
		{
			log.error("Error processing PRIVMSG: " + msg, e);
		}
	}

	public static void onCtcp(String creator, String recipient, String msg)
	{
		boolean stop = false;

		if (msg.startsWith("\1VERSION"))
		{
			User u = User.findUser(recipient);
			if (u instanceof AcidUser)
			{
				AcidUser au = (AcidUser) u;
				if (au.client != null && au.client.version != null)
					Acidictive.notice(au.getUID(), creator, "\1VERSION " + au.client.version + "\1");
			}
		}

		EventCTCP eventCtcp = new EventCTCP();
		eventCtcp.setCreator(creator);
		eventCtcp.setRecipient(recipient);
		eventCtcp.setMsg(msg);

		eventBus.post(eventCtcp);
	}

	public static void onCtcpReply(User source, String target, String message)
	{
		EventCTCPReply event = new EventCTCPReply();
		event.setSource(source);
		event.setTarget(target);
		event.setMessage(message);
		eventBus.post(event);
	}

	public static void onNotice(String creator, String recipient, String msg)
	{
		if (creator.equalsIgnoreCase("nickserv") && msg.matches("^.*registered\\s+and\\s+protected.*$"))
		{
			AcidUser au = (AcidUser) User.findUser(recipient);
			if (au != null && au.getNSPass() != null && !au.getNSPass().isEmpty())
				Protocol.privmsg(recipient, creator, "IDENTIFY " + au.getNSPass());
			return;
		}

		EventNotice eventNotice = new EventNotice();
		eventNotice.setCreator(creator);
		eventNotice.setRecipient(recipient);
		eventNotice.setMsg(msg);
		eventBus.post(eventNotice);
	}

	public static void onServerNotice(String source, String recipient, String msg)
	{
		EventServerNotice event = new EventServerNotice();
		event.setSource(source);
		event.setRecipient(recipient);
		event.setMsg(msg);
		eventBus.post(event);
	}

	public static void onDie(String msg)
	{
		log.info(msg);
	}

	public static void onSync()
	{
		syncTime = (int) new Date().getTime() - (int) startTime;
		log.info("Synced in " + (double) syncTime / 1000 + " seconds.");

		EventSync event = new EventSync();
		eventBus.post(event);
	}

	public static void onKill(final String killer, User user, final String reason)
	{
		EventKill eventKill = new EventKill();
		eventKill.setKiller(killer);
		eventKill.setUser(user);
		eventKill.setReason(reason);
		eventBus.post(eventKill);
	}

	public static int getUptimeTS()
	{
		return getTS() - (int) ((double) startTime / 1000);
	}

	public static String getUptime()
	{
		return Util.formatTime(getUptimeTS());
	}

	public static void onKick(String kicker, User victim, Channel channel, String reason)
	{
		EventKick eventKick = new EventKick();
		eventKick.setKicker(kicker);
		eventKick.setVictim(victim);
		eventKick.setChannel(channel);
		eventKick.setReason(reason);
		eventBus.post(eventKick);

		if (victim.getServer() == AcidCore.me)
		{
			AcidUser au = (AcidUser) victim;
			au.joinChan(channel.getName());
		}
	}

	public static void onInvite(User inviter, User invitee, Channel channel)
	{
		EventInvite eventInvite = new EventInvite();
		eventInvite.setInviter(inviter);
		eventInvite.setInvitee(invitee);
		eventInvite.setChannel(channel);
		eventBus.post(eventInvite);
	}

	public static void onMode(String creator, String recipient, String modes)
	{
	}

	public static void onChanMode(String creator, Channel chan, String modes)
	{
		String[] x = modes.split("\\s+");
		if (x.length >= 1)
		{
			boolean give = true;
			int whatnick = 1;
			String m;
			for (int i = 0; i < x[0].length(); i++)
			{
				m = x[0].substring(i, i + 1);

				if (m.equals("+"))
					give = true;
				else if (m.equals("-"))
					give = false;
				else if (m.matches("(b|I|e)"))
					whatnick++;
				else if (m.equals("k"))
				{
					if (give)
						chan.setKey(x[whatnick]);
					else
						chan.setKey(null);

					++whatnick;

					if (give)
						chan.setMode(m.charAt(0));
					else
						chan.unsetMode(m.charAt(0));
				}
				else if (m.contains("l"))
				{
					if (give)
					{
						chan.setLimit(Integer.parseInt(x[whatnick]));
						whatnick++;
					}
					else
						chan.setLimit(0);

					if (give)
						chan.setMode(m.charAt(0));
					else
						chan.unsetMode(m.charAt(0));
				}
				else if (m.matches("(v|h|o|a|q)"))
				{
					String targStr = x[whatnick];
					++whatnick;

					User target = User.findUser(targStr);
					if (target == null)
						continue;

					Membership mem = chan.findUser(target);
					if (mem == null)
						continue;

					if (give)
						mem.addMode(m);
					else
						mem.remMode(m);
				}
				else
				{
					if (give)
						chan.setMode(m.charAt(0));
					else
						chan.unsetMode(m.charAt(0));
				}
			}
		}

		EventChanMode event = new EventChanMode();
		event.setChan(chan);
		event.setModes(modes);
		event.setPrefix(creator);
		eventBus.post(event);
	}

	public static void onWebIRC(Server source, String operation, String uid, String realhost, String sockhost, String webircPassword, String webircUsername, String fakeHost, String fakeIp)
	{
		EventWebIRC event = new EventWebIRC();
		event.setSource(source);
		event.setOperation(operation);
		event.setUid(uid);
		event.setRealhost(realhost);
		event.setSockhost(sockhost);
		event.setWebircPassword(webircPassword);
		event.setWebircUsername(webircUsername);
		event.setFakeHost(fakeHost);
		event.setFakeIp(fakeIp);
		eventBus.post(event);
	}

	public static void onEncap(Server sourceServer, User sourceUser, Server target, String command, String[] arguments)
	{
		EventEncap event = new EventEncap(sourceServer, sourceUser, target, command, arguments);
		eventBus.post(event);
	}

	public static void setMode(String source, String target, String modes)
	{
		Channel c = Channel.findChannel(target);
		if (c != null)
			onChanMode(source, c, modes);

		// XXX
		String[] s = modes.split(" ");
		Object[] o = new Object[s.length];
		System.arraycopy(s, 0, o, 0, s.length);

		Protocol.mode(source, target, o);
	}

	// The only calling instance, KKill, already checks if user exists
	public static void skill(User u, String msg)
	{
		Protocol.kill(u.getUID(), msg);
		u.onQuit();
		onQuit(u, "Killed (" + me.getName() + " (" + msg + "))");
	}

	public static void onSquit(Server server)
	{
		quitStorm.onServerDelink(server);
	}

	public static void kick(AcidUser kicker, User kickee, Channel channel, final String reason)
	{
		channel.removeUser(kickee);
		kickee.remChan(channel);

		Protocol.kick(kicker, kickee, channel.getName(), reason);
		onKick(kicker.getNick(), kickee, channel, reason);
	}

	public static void privmsg(final Channel recipient, String message)
	{
		String crypto_pass = Acidictive.conf.getCryptoPass(recipient.getName());
		if (crypto_pass != null)
		{
			Blowfish bf = new Blowfish(crypto_pass);
			message = bf.Encrypt(message);
			log.debug("Blowfish encrypted:" + message);
		}

		Protocol.privmsg(conf.general.control, recipient.getName(), message);
	}

	public static void privmsg(final User recipient, String message)
	{
		Protocol.privmsg(conf.general.control, recipient.getUID(), message);
	}

	//@Deprecated This should be deprecated however I need the warnings to stfu atm in eclipse
	public static void privmsg(final String recipient, String message)
	{
		privmsg(conf.general.control, recipient, message);
	}

	public static void privmsg(String source, String target, String message)
	{
		if (target.startsWith("#"))
		{
			String crypto_pass = Acidictive.conf.getCryptoPass(target);
			if (crypto_pass != null)
			{
				Blowfish bf = new Blowfish(crypto_pass);
				message = bf.Encrypt(message);
				log.debug("Blowfish encrypted:" + message);
			}
		}
		Protocol.privmsg(source, target, message);
	}

	public static void notice(final Channel recipient, final String message)
	{
		Protocol.notice(conf.general.control, recipient.getName(), message);
	}

	public static void notice(final User recipient, final String message)
	{
		Protocol.notice(conf.general.control, recipient.getUID(), message);
	}

	public static void notice(final String source, final String recipient, final String message)
	{
		Protocol.notice(source, recipient, message);
	}

	//@Deprecated
	public static void notice(final String recipient, final String message)
	{
		Protocol.notice(conf.general.control, recipient, message);
	}

	//@Deprecated
	public static void sNotice(final String recipient, final String message)
	{
		Protocol.notice(me.getSID(), recipient, message);
	}

	public static void reply(String source, String target, String msg)
	{
		if (!target.startsWith("#"))
		// Reply to PMs in notice, reply to channel in privmsg
			/* TODO: How the hell do we avoid using notice(String,String)?
			 * Thinking of adding a "Target" interface to make this at least
			 * target-agnostic.
			 */
			notice(source, msg);
		else
			privmsg(target, msg);
	}

	public static void reply(User source, AcidUser to, Channel c, String message)
	{
		if (c != null && to != null)
			privmsg(to.getNick(), c.getName(), message);
		else if (to != null && c == null)
			notice(to.getNick(), source.getNick(), message);
		else if (c != null && to == null)
			privmsg(conf.general.control, c.getName(), message);
		else
			notice(conf.general.control, source.getNick(), message);
	}

	public static void loadClients(Plugin pkg, List<Client> clients)
	{
		for (Client c : clients)
		{
			User u = User.findUser(c.nick);
			if (u != null)
			{
				if (u instanceof AcidUser)
				{
					AcidUser au = (AcidUser) u;

					// update config and plugin
					au.client = c;
					au.pkg = pkg;
				}
				continue;
			}

			AcidUser au = new AcidUser(pkg, c);
			if (syncTime != 0)
				au.introduce();
		}
	}

	private static Runnable catchAll(Runnable r)
	{
		return () -> {
			try
			{
				r.run();
			}
			catch (Throwable ex)
			{
				log.warn("uncaught exception thrown out of task", ex);
			}
		};
	}

	public static ScheduledFuture scheduleWithFixedDelay(Runnable r, long t, TimeUnit unit)
	{
		ScheduledFuture future = eventLoop.scheduleWithFixedDelay(catchAll(r), t, t, unit);
		return future;
	}

	public static ScheduledFuture scheduleAtFixedRate(Runnable r, long t, TimeUnit unit)
	{
		ScheduledFuture future = eventLoop.scheduleAtFixedRate(catchAll(r), t, t, unit);
		return future;
	}

	public static ScheduledFuture schedule(Runnable r, long t, TimeUnit unit)
	{
		ScheduledFuture future = eventLoop.schedule(catchAll(r), t, unit);
		return future;
	}
}
