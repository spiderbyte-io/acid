package net.rizon.acid.core;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Server
{
	private static final Logger log = LoggerFactory.getLogger(Server.class);

	private String name, description, SID;
	private Server hub;
	private int linkTime, syncTime, users, recordusers, hops;
	private boolean burst;
	private Collection<Server> children = new HashSet<>();

	public Server(String name, Server hub, String description, int hops, String SID)
	{
		this.name = name;
		this.description = description;
		this.hub = hub;
		this.hops = hops;
		users = 0;
		recordusers = 0;
		linkTime = AcidCore.getTS();
		syncTime = 0;
		this.burst = true;
		this.SID = SID;

		addServerToMaps(this);

		if (hub != null)
		{
			hub.addChild(this);
		}

		log.debug("New server: " + name + " (SID: " + SID + ") uplinked to " + (hub != null ? hub.getName() : "nothing"));
	}

	public Collection<Server> getChildren()
	{
		return this.children;
	}

	public void addChild(Server server)
	{
		children.add(server);
	}

	public void removeChild(Server server)
	{
		children.remove(server);
	}

	public void onQuit()
	{
		removeServerFromMaps(this);
		hub.removeChild(this);
	}

	public String getSID()
	{
		return SID;
	}

	public int getRecordUsers()
	{
		return recordusers;
	}

	public void setRecordUsers(int n)
	{
		recordusers = n;
	}

	public String getName()
	{
		return name;
	}

	public String getDescription()
	{
		return description;
	}

	public int getUsers()
	{
		return users;
	}

	public void incUsers()
	{
		++this.users;
	}

	public void decUsers()
	{
		--this.users;
		if (this.users < 0)
		{
			this.users = 0;
			log.warn("User count for " + this.getName() + " managed to drop below 0?");
		}
	}

	public boolean isBursting()
	{
		return this.burst;
	}

	public Server getHub()
	{
		return hub;
	}

	public int getHops()
	{
		return hops;
	}

	public boolean isJupe()
	{
		return (hub == AcidCore.me || hub.isUlined());
	}

	@Override
	public String toString()
	{
		return getName();
	}

	public void finishBurst()
	{
		if (this.burst == false)
			return;

		this.syncTime = AcidCore.getTS();
		this.burst = false;

		for (Server serv : Server.getServersC())
		{
			if (serv.getHub() == this)
				serv.finishBurst();
		}

		// If this server is finished syncing and it is uplinked directoy to me,
		// then we are finished syncing
		if (this.getHub() == AcidCore.me)
			AcidCore.me.finishBurst();
	}

	public int getLinkCount()
	{
		int count = 1;

		for (Server serv : Server.getServersC())
		{
			if (serv.getHub() == this)
				count += serv.getLinkCount();
		}

		return count;
	}

	public int getRecursiveUsers()
	{
		int count = this.getUsers();

		for (Server serv : Server.getServersC())
		{
			if (serv.getHub() == this)
				count += serv.getRecursiveUsers();
		}

		return count;
	}

	public int getLinkTime()
	{
		return linkTime;
	}

	public int getSyncTime()
	{
		return syncTime;
	}

	public boolean isUlined()
	{
		for (String s : Acidictive.conf.general.ulines)
			if (this.getName().equalsIgnoreCase(s))
				return true;
		return false;
	}

	private static HashMap<String, Server> sidMap = new HashMap<String, Server>();
	private static HashMap<String, Server> serverMap = new HashMap<String, Server>();

	public static Server findServer(final String name)
	{
		if (name.length() == 3 && name.indexOf('.') == -1)
			return sidMap.get(name);
		return serverMap.get(name.toLowerCase());
	}

	public static final Set<String> getServers()
	{
		return serverMap.keySet();
	}

	public static Collection<Server> getServersC()
	{
		return serverMap.values();
	}

	private static void addServerToMaps(Server server)
	{
		sidMap.put(server.getSID(), server);
		serverMap.put(server.getName(), server);
	}

	private static void removeServerFromMaps(Server server)
	{
		sidMap.remove(server.getSID());
		serverMap.remove(server.getName());
	}

	public static final String toName(final String sid)
	{
		Server s = findServer(sid);
		if (s != null)
			return s.getName();
		return sid;
	}

	public static int serverCount()
	{
		return serverMap.size();
	}

	public static Server[] sort(String method)
	{
		class ServerUsersComparator implements Comparator<Server>
		{
			@Override
			public int compare(Server s1, Server s2)
			{
				if (s1.getUsers() == s2.getUsers())
					return 0;
				if (s1.getUsers() > s2.getUsers())
					return -1;
				return 1;
			}
		}
		class ServerNameComparator implements Comparator<Server>
		{
			@Override
			public int compare(Server s1, Server s2)
			{
				return s1.getName().compareTo(s2.getName());
			}
		}
		Comparator<Server> comp;
		if (method.equals("users"))
			comp = new ServerUsersComparator();
		else
			comp = new ServerNameComparator();
		Server e[] = new Server[serverMap.size()];
		serverMap.values().toArray(e);
		Arrays.sort(e, comp);
		return e;
	}

	public static String format(Server[] servers)
	{
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < servers.length; i++)
		{
			buf.append("(" + i + ")" + servers[i] + "\n");
		}
		return buf.toString();
	}
}
