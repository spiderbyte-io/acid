package net.rizon.acid.core;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

public class UserList
{
	private static final Hashtable<String, Integer> ipList = new Hashtable<String, Integer>();

	public static User[] findUsersByHost(String ip)
	{
		if (ip == null || ip.equalsIgnoreCase(""))
			return null;
		Boolean IP = ip.matches("^(\\d{1,3}\\.){3}\\d{1,3}$");
		ArrayList<User> capUsers = new ArrayList<User>();
		for (User u : User.getUsersC())
		{
			if ((IP && u.getIP().equals(ip)) || (!IP && u.getHost().equals(ip)))
				capUsers.add(u);
		}
		if (capUsers.size() > 0)
		{
			User[] usrs = new User[capUsers.size()];
			for (int i = 0; i < capUsers.size(); i++)
				usrs[i] = capUsers.get(i);
			return usrs;
		}
		return null;

	}

	public static User locateNick(String nick)
	{
		for (User x : User.getUsersC())
		{
			ArrayList<String> n = x.oldNicks();
			for (int e = 0; e < n.size(); e++)
			{
				if (n.get(e).equalsIgnoreCase(nick))
					return x;
			}
		}
		return null;
	}

	public static int identedNicks()
	{
		int num = 0;
		for (User x : User.getUsersC())
		{
			if (x.hasMode("r"))
				num++;
		}
		return num;
	}

	public static void flushNicks()
	{
		for (User x : User.getUsersC())
			x.flush();
	}

	public static void clearChans()
	{
		for (User x : User.getUsersC())
			x.clearFlood();
	}

	public static String globToRegex(String glob)
	{
		StringBuilder regex = new StringBuilder("^");
		for (char c : glob.toCharArray())
		{
			switch (c)
			{
				case '*':
					regex.append(".*");
					break;
				case '?':
					regex.append(".");
					break;
				case '{':
				case '}':
				case '(':
				case ')':
				case '\\':
				case '|':
				case '+':
				case '.':
				case '^':
				case '$':
					regex.append('\\');
				default:
					regex.append(c);
					break;
			}
		}
		return regex.append("$").toString();
	}

	public static void increaseHost(final String ip)
	{
		Integer hosts = ipList.get(ip);
		if (hosts == null)
			ipList.put(ip, 1);
		else
			ipList.put(ip, hosts + 1);
	}

	public static void decreaseHost(final String ip)
	{
		int hosts = ipList.get(ip);
		if (hosts == 1)
			ipList.remove(ip);
		else
			ipList.put(ip, hosts - 1);
	}

	public static int getUserCountForIP(final String ip)
	{
		Integer hosts = ipList.get(ip);
		return (hosts == null ? 0 : hosts);
	}
}
