package net.rizon.acid.messages;

import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.User;

public class Invite extends Message
{
	public Invite()
	{
		super("INVITE");
	}

	// :42HAAAAAC INVITE 44SAAAAAA #NASA 1348235231

	@Override
	public void onUser(User u, String[] params)
	{
		User invitee = User.findUser(params[0]);
		Channel channel = Channel.findChannel(params[1]);
		int ts = Integer.parseInt(params[2]);

		if (invitee == null || channel == null || channel.getTS() != ts)
			return;

		Acidictive.onInvite(u, invitee, channel);
	}
}
