package net.rizon.acid.messages;

import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.User;

public class Nick extends Message
{
	public Nick()
	{
		super("NICK");
	}

	// :99hAAAAAB NICK wild :1233188135

	@Override
	public void onUser(User u, String[] params)
	{
		String oldnick = u.getNick();
		u.changeNick(params[0]);
		u.changeNickTS(Integer.parseInt(params[1]));

		Acidictive.onNickChange(u, oldnick);
	}
}