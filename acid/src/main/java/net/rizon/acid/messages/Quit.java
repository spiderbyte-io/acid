package net.rizon.acid.messages;

import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.User;

public class Quit extends Message
{
	public Quit()
	{
		super("QUIT");
	}

	// :99hAAAAAA QUIT :Quit: moo

	@Override
	public void onUser(User x, String[] params)
	{
		Acidictive.onQuit(x, params[0]);
		x.onQuit();
	}
}