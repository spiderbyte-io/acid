package net.rizon.acid.messages;

import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SID extends Message
{
	private static final Logger log = LoggerFactory.getLogger(SID.class);

	public SID()
	{
		super("SID");
	}

	// :8OL SID irc.lolinator.net 6 17C :Rizon Client Server
	// :4SS SID services.l2thorn.com 2 00H :Services for L2Thorn IRC Network
	// :4SS SID irc.dev.cccp-project.net 2 99S :Communist Bitch ass Monkey whore

	@Override
	public void onServer(Server from, String[] params)
	{
		Server server = Server.findServer(params[0]);
		if (server == null)
			server = Server.findServer(params[2]);
		if (server != null)
		{
			log.warn("Server " + server.getName() + " (" + params[2] + "/" + server.getSID() + ") introduced from " + from.getName() + ", but already exists from " + (server.getHub() != null ? server.getHub().getName() : " no uplink"));
			return;
		}

		server = new Server(params[0], from, params[3], Integer.parseInt(params[1]), params[2]);
		Acidictive.onServer(server);
	}
}