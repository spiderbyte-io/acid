package net.rizon.acid.messages;

import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.Server;
import net.rizon.acid.core.User;
import net.rizon.acid.core.UserList;

public class UID extends Message
{
	public UID()
	{
		super("UID");
	}

	// :00C UID Adam 1 1317939198 +aiow ~Adam 192.168.1.2 192.168.1.2 00CAAAAAA 0 192.168.1.2 :Adam
	// source UID 0-nick 1-hops 2-ts 3-umode 4-username 5-hostname 6-ip 7-uid 8-servicesstamp 9-realhost 10-realname

	@Override
	public void onServer(Server source, String[] params)
	{
		User user = new User(params[0], params[4], params[9], params[5], params[10], source, Integer.parseInt(params[2]), Integer.parseInt(params[2]), params[3], params[7], params[6]);
		UserList.increaseHost(params[6]);
		if (source.isBursting() == false)
			Acidictive.onNick(user);
	}
}