package net.rizon.acid.util;

import java.util.Date;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import net.rizon.acid.core.Acidictive;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Handles sending e-mails.
 */
public class EMail
{
	private static final Logger log = LoggerFactory.getLogger(EMail.class);

	public EMail(String to, String subject, String msg)
	{
		Properties mailprops = new Properties();
		mailprops.put("mail.smtp.host", Acidictive.conf.general.email_smtp);
		if (Acidictive.conf.debug)
			mailprops.put("mail.debug", "true");

		Session session = Session.getInstance(mailprops);

		try
		{
			Message m = new MimeMessage(session);
			m.setFrom(new InternetAddress(Acidictive.conf.general.email_from));
			m.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
			m.setSubject(subject);
			m.setSentDate(new Date());
			m.setText(msg);
			Transport.send(m);
		}
		catch(MessagingException e)
		{
			log.error(null, e);
		}
	}
}
