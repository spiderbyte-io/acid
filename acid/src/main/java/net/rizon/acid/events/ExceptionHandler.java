package net.rizon.acid.events;

import com.google.common.eventbus.SubscriberExceptionContext;
import com.google.common.eventbus.SubscriberExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExceptionHandler implements SubscriberExceptionHandler
{
	private static final Logger logger = LoggerFactory.getLogger(ExceptionHandler.class);

	@Override
	public void handleException(Throwable exception, SubscriberExceptionContext context)
	{
		logger.error("exception thrown from event", exception);
	}

}
