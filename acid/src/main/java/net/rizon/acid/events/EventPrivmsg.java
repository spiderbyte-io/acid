package net.rizon.acid.events;

public class EventPrivmsg
{
	private String creator;
	private String recipient;
	private String msg;

	private boolean stopProcessing;

	public String getCreator()
	{
		return creator;
	}

	public void setCreator(String creator)
	{
		this.creator = creator;
	}

	public String getRecipient()
	{
		return recipient;
	}

	public void setRecipient(String recipient)
	{
		this.recipient = recipient;
	}

	public String getMsg()
	{
		return msg;
	}

	public void setMsg(String msg)
	{
		this.msg = msg;
	}

	public boolean isStopProcessing()
	{
		return stopProcessing;
	}

	public void setStopProcessing(boolean stopProcessing)
	{
		this.stopProcessing = stopProcessing;
	}
}
