package net.rizon.acid.commands;

import net.rizon.acid.core.AcidUser;
import net.rizon.acid.core.Acidictive;
import static net.rizon.acid.core.Acidictive.reply;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Command;
import net.rizon.acid.core.Server;
import net.rizon.acid.core.User;
import net.rizon.acid.core.UserList;
import net.rizon.acid.core.Version;

/**
 * Shows stats.
 */
public class Stats extends Command
{
	public Stats()
	{
		super(0, 0);
	}

	@Override
	public void Run(User x, AcidUser to, Channel c, final String[] args)
	{
		reply(x, to, c, "Running " + Acidictive.getVersion() + " from " + Version.GIT_AUTHOR_DATE + " by " + Version.GIT_AUTHOR + ", Uptime: " + Acidictive.getUptime());
		reply(x, to, c, "Memory: " + (Runtime.getRuntime().freeMemory() / 1024) + " Kb free, " + (Runtime.getRuntime().totalMemory() / 1024) + " Kb total, " + (Runtime.getRuntime().maxMemory() / 1024) + " Kb max.");
		reply(x, to, c, "Uptime: " + Acidictive.getUptime() + "; Threads: " + Thread.activeCount());
		reply(x, to, c, "Current Users: " + User.userCount() + " Identified: " +
			UserList.identedNicks() + "; Servers: " + Server.serverCount());
	}

	@Override
	public void onHelp(User u, AcidUser to, Channel c)
	{
		Acidictive.reply(u, to, c, "\2stats\2 / Shows miscellaneous info about " + to.getNick());
	}

	@Override
	public boolean onHelpCommand(User u, AcidUser to, Channel c)
	{
		Acidictive.reply(u, to, c, "Syntax: \2stats\2");
		Acidictive.reply(u, to, c, " ");
		Acidictive.reply(u, to, c, "The stats command allows you to retrieve info about " + to.getNick() + ".");
		Acidictive.reply(u, to, c, "Information shown includes uptime, memory usage, thread count, the");
		Acidictive.reply(u, to, c, "current amount of users and servers on the network");
		return true;
	}
}
