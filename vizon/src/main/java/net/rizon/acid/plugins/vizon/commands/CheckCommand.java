/*
 * Copyright (c) 2017, orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.plugins.vizon.commands;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import net.rizon.acid.core.AcidUser;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Command;
import net.rizon.acid.core.User;
import net.rizon.acid.plugins.vizon.Vizon;
import net.rizon.acid.plugins.vizon.VizonBet;
import net.rizon.acid.plugins.vizon.db.VizonDrawing;
import net.rizon.acid.plugins.vizon.db.VizonUser;

/**
 *
 * @author orillion <orillion@rizon.net>
 */
public class CheckCommand extends Command
{
	public CheckCommand()
	{
		super(1, 1);
	}

	@Override
	public void Run(User source, AcidUser to, Channel c, String[] args)
	{
		if (!source.isIdentified())
		{
			// User has not identified to NickServ.
			Acidictive.reply(source, to, c, "You need to register your nickname before you can use this command");
			return;
		}

		int drawingId;

		try
		{
			drawingId = Integer.parseInt(args[0]);
		}
		catch (NumberFormatException e)
		{
			Acidictive.reply(source, to, c, "Please specify the number of the drawing you wish to check");
			return;
		}

		VizonDrawing drawing = Vizon.getVizonDatabase().getDrawingById(drawingId);

		if (drawing == null)
		{
			Acidictive.reply(source, to, c, String.format("Drawing with id %d does not exist", drawingId));
			return;
		}

		VizonUser user = Vizon.getVizonDatabase().findOrCreateUser(source.getNick());

		if (user == null)
		{
			// Should not happen, but just in case.
			Acidictive.reply(source, to, c, "Unable to find your name in the database");
			return;
		}

		VizonBet bet = Vizon.getVizonDatabase().findBetForUserAndDrawing(user, drawing);

		if (drawing.getDate().isAfter(LocalDateTime.now()))
		{
			if (bet == null)
			{
				Acidictive.reply(source, to, c, String.format(
						"Drawing No.%d did not happen yet!",
						drawingId));
			}
			else
			{
				Acidictive.reply(source, to, c, String.format(
						"Drawing No.%d did not happen yet! Your bet is: %d %d %d %d %d %d",
						drawingId,
						bet.getBet().getFirst(),
						bet.getBet().getSecond(),
						bet.getBet().getThird(),
						bet.getBet().getFourth(),
						bet.getBet().getFifth(),
						bet.getBet().getSixth()));
			}

			return;
		}

		List<Integer> bets = drawing.getDraws();
		Collections.sort(bets);

		if (bet == null)
		{
			Acidictive.reply(source, to, c, String.format(
					"The result of VIzon No.%d was: %d %d %d %d %d %d, you did not place a bet for this drawing",
					drawing.getId(),
					bets.get(0),
					bets.get(1),
					bets.get(2),
					bets.get(3),
					bets.get(4),
					bets.get(5)));
		}
		else
		{
			// @TODO: Figure out how to recover Grand Prize info, perhaps
			//        store in the database or something.
			int correct = drawing.checkCorrect(bet.getBet());
			String formatted_message = "The result of VIzon No.%d was: %d %d %d %d %d %d, ";

			switch (correct)
			{
				case 6:
					formatted_message += "you won first prize for this drawing! Your bet was: %d %d %d %d %d %d";
					break;
				case 5:
					formatted_message += "you won second prize for this drawing! Your bet was: %d %d %d %d %d %d";
					break;
				case 4:
					formatted_message += "you won third prize for this drawing! Your bet was: %d %d %d %d %d %d";
					break;
				case 3:
					formatted_message += "you won the consolation prize for this drawing! Your bet was: %d %d %d %d %d %d";
					break;
				default:
					formatted_message += "you did not win any prize for this drawing. Your bet was: %d %d %d %d %d %d";
					break;
			}

			Acidictive.reply(source, to, c, String.format(formatted_message,
							drawing.getId(),
							bets.get(0),
							bets.get(1),
							bets.get(2),
							bets.get(3),
							bets.get(4),
							bets.get(5),
							bet.getBet().getFirst(),
							bet.getBet().getSecond(),
							bet.getBet().getThird(),
							bet.getBet().getFourth(),
							bet.getBet().getFifth(),
							bet.getBet().getSixth()));
		}
	}
}
