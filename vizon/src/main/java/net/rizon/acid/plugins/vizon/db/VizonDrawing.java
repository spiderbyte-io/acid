/*
 * Copyright (c) 2017, orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.plugins.vizon.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import net.rizon.acid.plugins.vizon.Bet;
import net.rizon.acid.plugins.vizon.DrawingState;
import net.rizon.acid.plugins.vizon.Vizon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author orillion <orillion@rizon.net>
 */
public class VizonDrawing
{
	private static final Logger logger = LoggerFactory.getLogger(VizonDrawing.class);

	private final int id;
	private final List<Integer> draws;
	private final LocalDateTime date;

	/**
	 * Constructs a new {@link VizonDrawing} from the supplied
	 * {@link ResultSet}.
	 *
	 * @param rs {@link ResultSet} to construct object from.
	 *
	 * @return {@link VizonDrawing} or null if unable to construct object.
	 */
	public static VizonDrawing fromResultSet(ResultSet rs)
	{
		try
		{
			// Grab id
			int id = rs.getInt("id");

			// Grab drawn numbers, ints default to 0 when they are NULL in
			// the database, and we cannot bet 0, so this is okay for now.
			// Should this change in the future, use rs.wasNull().
			int first = rs.getInt("first");
			int second = rs.getInt("second");
			int third = rs.getInt("third");
			int fourth = rs.getInt("fourth");
			int fifth = rs.getInt("fifth");
			int sixth = rs.getInt("sixth");

			// Grab date this drawing is supposed to take place, or took place
			// depending on what time it is now.
			Timestamp ts = rs.getTimestamp("drawing_date");
			List<Integer> draws = Arrays.asList(
					first,
					second,
					third,
					fourth,
					fifth,
					sixth);

			return new VizonDrawing(id, draws, ts.toLocalDateTime());
		}
		catch (SQLException ex)
		{
			logger.warn("Unable to construct VizonDrawing object", ex);
			return null;
		}
	}

	private VizonDrawing(int id, List<Integer> draws, LocalDateTime date)
	{
		this.id = id;
		this.draws = draws;
		this.date = date;
	}

	public int getId()
	{
		return id;
	}

	public List<Integer> getDraws()
	{
		return draws;
	}

	public LocalDateTime getDate()
	{
		return date;
	}

	public int checkCorrect(Bet bet)
	{
		return checkCorrect(bet.asList());
	}

	public int checkCorrect(List<Integer> bets)
	{
		int correct = 0;

		// Check how many of the bets are in the draws.
		for (int bet : bets)
		{
			if (draws.contains(bet))
			{
				correct++;
			}
		}

		return correct;
	}

	public void setDrawingResult(Bet result)
	{
		this.draws.set(0, result.getFirst());
		this.draws.set(1, result.getSecond());
		this.draws.set(2, result.getThird());
		this.draws.set(3, result.getFourth());
		this.draws.set(4, result.getFifth());
		this.draws.set(5, result.getSixth());
	}

	public DrawingState getState()
	{
		LocalDateTime now = LocalDateTime.now();

		if (getOpenDate().isAfter(now))
		{
			return DrawingState.PREPARING;
		}
		else if (getCloseDate().isAfter(now))
		{
			return DrawingState.OPEN;
		}
		else
		{
			return DrawingState.CLOSED;
		}
	}

	public LocalDateTime getOpenDate()
	{
		Duration before = Vizon.getConf().getOpenTimeInterval();

		return this.date.minus(before);
	}

	public LocalDateTime getCloseDate()
	{
		Duration before = Vizon.getConf().getCloseTimeInterval();

		return this.date.minus(before);
	}
}
