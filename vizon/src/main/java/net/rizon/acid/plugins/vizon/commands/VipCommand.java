/*
 * Copyright (c) 2017, orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.plugins.vizon.commands;

import java.time.Duration;
import java.time.LocalDateTime;
import net.rizon.acid.core.AcidUser;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Command;
import net.rizon.acid.core.User;
import net.rizon.acid.plugins.vizon.Vizon;
import net.rizon.acid.plugins.vizon.db.VizonUser;
import net.rizon.acid.util.Util;

/**
 *
 * @author orillion <orillion@rizon.net>
 */
public class VipCommand extends Command
{
	private static final String VIP_FORMAT = "Multiplication factor: %d; Total accumulated days of current VIP: %d; Last drawing claimed of current VIP: %s (No.%d); Remaining time of current VIP: %s";
	private static final String PERMANENT_VIP_FORMAT = "Congratulations, your colored vhost is permanent! Last drawing claimed of current VIP: %s (No.%d)";

	public VipCommand()
	{
		super(0, 0);
	}

	@Override
	public void Run(User source, AcidUser to, Channel c, String[] args)
	{
		if (!source.isIdentified())
		{
			// User not identified
			Acidictive.reply(source, to, c, "You need to identify before you can use this command");
			return;
		}

		VizonUser user = Vizon.getVizonDatabase().findUser(source.getNick());

		if (user == null)
		{
			// User never played
			Acidictive.reply(source, to, c, "You have never played in Vizon before!");
			return;
		}

		if (user.getObtained() == null)
		{
			// Not in a VIP session
			Acidictive.reply(source, to, c, "You have no VIP");
			return;
		}

		if (user.isPermanent())
		{
			Acidictive.reply(source, to, c, String.format(
					PERMANENT_VIP_FORMAT,
					user.getObtained(),
					user.getObtainedId()));
		}
		else
		{
			Duration remaining = Duration.between(LocalDateTime.now(), user.getObtained().plusDays(user.getDays()));
			String remainingTime = Util.fTime((int) remaining.getSeconds()).trim();

			Acidictive.reply(source, to, c, String.format(
					VIP_FORMAT,
					user.getMultiplier(),
					user.getDays(),
					user.getObtained(),
					user.getObtainedId(),
					remainingTime));
		}
	}
}
