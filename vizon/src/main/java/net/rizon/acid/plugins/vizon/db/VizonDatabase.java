/*
 * Copyright (c) 2017, orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.plugins.vizon.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import net.rizon.acid.plugins.vizon.Bet;
import net.rizon.acid.plugins.vizon.RequestStatus;
import net.rizon.acid.plugins.vizon.VizonBet;
import net.rizon.acid.sql.SQL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author orillion <orillion@rizon.net>
 */
public class VizonDatabase
{
	private static final Logger logger = LoggerFactory.getLogger(VizonDatabase.class);
	private final SQL vizonSql;

	public VizonDatabase(SQL vizonSql)
	{
		this.vizonSql = vizonSql;
	}

	/**
	 * Finds a drawing by its id.
	 *
	 * @param id Id of the drawing.
	 *
	 * @return {@link VizonDrawing} or null if not found.
	 */
	public VizonDrawing getDrawingById(int id)
	{
		try
		{
			PreparedStatement statement = vizonSql.prepare("SELECT * FROM vizon_drawings "
					+ "WHERE id = ?");

			statement.setInt(1, id);

			ResultSet rs = vizonSql.executeQuery(statement);

			if (rs.next())
			{
				return VizonDrawing.fromResultSet(rs);
			}

			return null;
		}
		catch (SQLException ex)
		{
			logger.warn("Unable to get next drawing in Vizon Database", ex);
			return null;
		}
	}

	/**
	 * Finds the drawing closest to the specified date.
	 *
	 * @param date Date to look up.
	 *
	 * @return {@link VizonDrawing} or null if none can be found.
	 */
	public VizonDrawing getDrawingByDate(LocalDateTime date)
	{
		if (date == null)
		{
			return null;
		}

		try
		{
			PreparedStatement statement = vizonSql.prepare("SELECT * FROM vizon_drawings "
					+ "ORDER BY ABS(TIMESTAMPDIFF(second, drawing_date, ?) "
					+ "LIMIT 1");

			statement.setTimestamp(1, Timestamp.valueOf(date));

			ResultSet rs = vizonSql.executeQuery(statement);

			if (rs.next())
			{
				return VizonDrawing.fromResultSet(rs);
			}

			return null;
		}
		catch (SQLException ex)
		{
			logger.warn("Unable to get next drawing in Vizon Database", ex);
			return null;
		}
	}

	public VizonDrawing getLatestDrawing()
	{
		try
		{
			PreparedStatement statement = vizonSql.prepare("SELECT * FROM vizon_drawings "
					+ "ORDER BY id DESC "
					+ "LIMIT 1");

			ResultSet rs = vizonSql.executeQuery(statement);

			if (rs.next())
			{
				return VizonDrawing.fromResultSet(rs);
			}

			return null;
		}
		catch (SQLException ex)
		{
			logger.warn("Unable to get next drawing in Vizon Database", ex);
			return null;
		}
	}

	/**
	 * Gets the next drawing that's scheduled to take place.
	 *
	 * @return {@link VizonDrawing} or null if nothing is scheduled.
	 */
	public VizonDrawing getNextDrawing()
	{
		try
		{
			PreparedStatement statement = vizonSql.prepare("SELECT * FROM vizon_drawings "
					+ "WHERE drawing_date >= NOW() "
					+ "ORDER BY drawing_date DESC "
					+ "LIMIT 1");

			ResultSet rs = vizonSql.executeQuery(statement);

			if (rs.next())
			{
				return VizonDrawing.fromResultSet(rs);
			}

			return null;
		}
		catch (SQLException ex)
		{
			logger.warn("Unable to get next drawing in Vizon Database", ex);
			return null;
		}
	}

	public VizonUser findUserById(int id)
	{
		try
		{
			PreparedStatement statement = vizonSql.prepare("SELECT * FROM vizon_users "
					+ "WHERE id = ?");

			statement.setInt(1, id);

			ResultSet rs = vizonSql.executeQuery(statement);

			if (rs.next())
			{
				return VizonUser.fromResultSet(rs);
			}

			return null;
		}
		catch (SQLException ex)
		{
			logger.warn("Unable to find user by id in Vizon Database", ex);
			return null;
		}
	}

	public VizonUser findUser(String nick)
	{
		if (nick == null)
		{
			return null;
		}

		try
		{
			PreparedStatement statement = vizonSql.prepare("SELECT * FROM vizon_users WHERE nick = ?");
			statement.setString(1, nick);

			ResultSet rs = vizonSql.executeQuery(statement);

			if (rs.next())
			{
				return VizonUser.fromResultSet(rs);
			}

			return null;
		}
		catch (SQLException ex)
		{
			logger.warn("Unable to select or create user in Vizon Database", ex);
			return null;
		}
	}

	/**
	 * Finds or creates a user. A user's id and nick are both unique in the
	 * database.
	 *
	 * @param nick Nick of the user. (Case insensitive)
	 *
	 * @return {@link VizonUser} or null if an error occurred.
	 */
	public VizonUser findOrCreateUser(String nick)
	{
		if (nick == null)
		{
			return null;
		}

		try
		{
			PreparedStatement statement = vizonSql.prepare("SELECT * FROM vizon_users WHERE nick = ?");
			statement.setString(1, nick);

			ResultSet rs = vizonSql.executeQuery(statement);

			if (rs.next())
			{
				return VizonUser.fromResultSet(rs);
			}

			statement = vizonSql.prepare("INSERT INTO vizon_users (nick) VALUES(?)");
			statement.setString(1, nick);

			int result = vizonSql.executeUpdateBlocking(statement);

			if (result != 1)
			{
				// Unable to insert new user.
				return null;
			}

			statement = vizonSql.prepare("SELECT * FROM vizon_users WHERE nick = ?");
			statement.setString(1, nick);

			rs = vizonSql.executeQuery(statement);

			if (rs.next())
			{
				return VizonUser.fromResultSet(rs);
			}

			return null;
		}
		catch (SQLException ex)
		{
			logger.warn("Unable to select or create user in Vizon Database", ex);
			return null;
		}
	}

	public VizonRequest findVhostRequestByUserId(int userId)
	{
		try
		{
			PreparedStatement statement = vizonSql.prepare("SELECT vizon_requests.*, vizon_users.nick FROM vizon_requests "
					+ "INNER JOIN vizon_users "
					+ "ON vizon_users.id = vizon_requests.user_id "
					+ "WHERE user_id = ? "
					+ "AND status = ?");
			statement.setInt(1, userId);
			statement.setInt(2, RequestStatus.PENDING);

			ResultSet rs = vizonSql.executeQuery(statement);

			if (rs.next())
			{
				return VizonRequest.fromResultSet(rs);
			}

			return null;
		}
		catch (SQLException ex)
		{
			logger.warn("Unable to find vhost request for nick", ex);
			return null;
		}
	}

	public VizonRequest findVhostRequest(String nick)
	{
		if (nick == null)
		{
			return null;
		}

		try
		{
			PreparedStatement statement = vizonSql.prepare("SELECT vizon_requests.*, vizon_users.nick FROM vizon_requests"
					+ "INNER JOIN vizon_users "
					+ "ON vizon_users.id = vizon_requests.user_id"
					+ "WHERE vizon_users.nick = ?");
			statement.setString(1, nick);

			ResultSet rs = vizonSql.executeQuery(statement);

			if (rs.next())
			{
				return VizonRequest.fromResultSet(rs);
			}

			return null;
		}
		catch (SQLException ex)
		{
			logger.warn("Unable to find vhost request for nick", ex);
			return null;
		}
	}

	public Collection<VizonRequest> findPendingVhostRequests()
	{
		try
		{
			PreparedStatement statement = vizonSql.prepare("SELECT vizon_requests.*, vizon_users.nick FROM vizon_requests "
					+ "INNER JOIN vizon_users "
					+ "ON vizon_users.id = vizon_requests.user_id "
					+ "WHERE vizon_requests.status = ?");
			statement.setInt(1, RequestStatus.PENDING);

			ResultSet rs = vizonSql.executeQuery(statement);

			List<VizonRequest> requests = new ArrayList<>();

			while (rs.next())
			{
				VizonRequest request = VizonRequest.fromResultSet(rs);

				if (request != null)
				{
					requests.add(request);
				}
			}

			return requests;
		}
		catch (SQLException ex)
		{
			logger.warn("Unable to find vhost request for nick", ex);
			return null;
		}
	}

	public boolean insertRequest(VizonRequest request)
	{
		if (request == null)
		{
			return false;
		}

		try
		{
			PreparedStatement statement = vizonSql.prepare("INSERT INTO vizon_requests "
					+ "(user_id, vhost, status, reason, oper) "
					+ "VALUES (?, ?, ?, ?, ?)");
			statement.setInt(1, request.getUserId());
			statement.setString(2, request.getVhost());
			statement.setInt(3, request.getStatus());
			statement.setString(4, request.getReason());
			statement.setString(5, request.getOper());

			int result = vizonSql.executeUpdateBlocking(statement);

			return result > 0;
		}
		catch (SQLException ex)
		{
			logger.warn("Unable to insert vhost request", ex);
			return false;
		}
	}

	public boolean updateRequest(VizonRequest request)
	{
		if (request == null)
		{
			return false;
		}

		try
		{
			PreparedStatement statement = vizonSql.prepare("UPDATE vizon_requests "
					+ "SET vhost = ?, "
					+ "status = ?, "
					+ "reason = ?, "
					+ "oper = ? "
					+ "WHERE id = ?");
			statement.setString(1, request.getVhost());
			statement.setInt(2, request.getStatus());
			statement.setString(3, request.getReason());
			statement.setString(4, request.getOper());
			statement.setInt(5, request.getId());

			int result = vizonSql.executeUpdateBlocking(statement);

			return true;
		}
		catch (SQLException ex)
		{
			logger.warn("Unable to insert vhost request", ex);
			return false;
		}
	}

	public boolean updateUser(VizonUser user)
	{
		if (user == null)
		{
			return false;
		}

		try
		{
			PreparedStatement statement = vizonSql.prepare("UPDATE vizon_users "
					+ "SET vhost = ?, "
					+ "eligible = ?, "
					+ "bold = ?, "
					+ "obtained = ?, "
					+ "obtained_id = ?, "
					+ "multiplier = ?, "
					+ "jackpot = ?, "
					+ "permanent = ?, "
					+ "days = ? "
					+ "WHERE id = ?");

			statement.setString(1, user.getVhost());
			statement.setBoolean(2, user.isEligible());
			statement.setBoolean(3, user.isBold());
			statement.setTimestamp(4, Timestamp.valueOf(user.getObtained()));
			statement.setInt(5, user.getObtainedId());
			statement.setInt(6, user.getMultiplier());
			statement.setBoolean(7, user.isJackpot());
			statement.setBoolean(8, user.isPermanent());
			statement.setInt(9, user.getDays());
			statement.setInt(10, user.getId());

			int updated = vizonSql.executeUpdateBlocking(statement);

			return updated > 0;
		}
		catch (SQLException ex)
		{
			logger.warn("Unable to select bet for user and drawing in Vizon Database", ex);
			return false;
		}
	}

	public boolean updateDrawing(VizonDrawing drawing)
	{
		if (drawing == null)
		{
			return false;
		}

		try
		{
			PreparedStatement statement = vizonSql.prepare("UPDATE vizon_drawings "
					+ "SET first = ?, "
					+ "second = ?, "
					+ "third = ?, "
					+ "fourth = ?, "
					+ "fifth = ?, "
					+ "sixth = ? "
					+ "WHERE id = ?");

			statement.setInt(1, drawing.getDraws().get(0));
			statement.setInt(2, drawing.getDraws().get(1));
			statement.setInt(3, drawing.getDraws().get(2));
			statement.setInt(4, drawing.getDraws().get(3));
			statement.setInt(5, drawing.getDraws().get(4));
			statement.setInt(6, drawing.getDraws().get(5));
			statement.setInt(7, drawing.getId());

			int updated = vizonSql.executeUpdateBlocking(statement);

			return updated > 0;
		}
		catch (SQLException ex)
		{
			logger.warn("Unable to select bet for user and drawing in Vizon Database", ex);
			return false;
		}
	}

	/**
	 * Attempts to find a {@link VizonBet} of the user for the specified
	 * drawing.
	 *
	 * @param user    User to find for.
	 * @param drawing Drawing to find for.
	 *
	 * @return {@link VizonBet} or null if none can be found.
	 */
	public VizonBet findBetForUserAndDrawing(VizonUser user, VizonDrawing drawing)
	{
		if (user == null || drawing == null)
		{
			return null;
		}

		try
		{
			PreparedStatement statement = vizonSql.prepare("SELECT * FROM vizon_bets WHERE vizon_users_id = ? AND vizon_drawings_id = ?");
			statement.setInt(1, user.getId());
			statement.setInt(2, drawing.getId());

			ResultSet rs = vizonSql.executeQuery(statement);

			if (rs.next())
			{
				return VizonBet.fromResultSet(rs);
			}

			return null;
		}
		catch (SQLException ex)
		{
			logger.warn("Unable to select bet for user and drawing in Vizon Database", ex);
			return null;
		}
	}

	public List<VizonBet> findBetsForUser(VizonUser user)
	{
		return null;
	}

	public List<VizonBet> findBetsForDrawing(VizonDrawing drawing)
	{
		List<VizonBet> bets = new ArrayList<>();

		if (drawing == null)
		{
			return bets;
		}

		try
		{
			PreparedStatement statement = vizonSql.prepare("SELECT * FROM vizon_bets WHERE vizon_drawings_id = ?");
			statement.setInt(1, drawing.getId());

			ResultSet rs = vizonSql.executeQuery(statement);

			while (rs.next())
			{
				VizonBet bet = VizonBet.fromResultSet(rs);

				if (bet == null)
				{
					continue;
				}

				bets.add(bet);
			}
		}
		catch (SQLException ex)
		{
			logger.warn("Unable to select bets for drawing in Vizon Database", ex);
		}

		return bets;
	}

	/**
	 * Creates a new bet for the user for the specified drawing.
	 *
	 * @param user    User to place the bet for.
	 * @param drawing Drawing to place the bet in.
	 * @param bet     Bet to place.
	 *
	 * @return True if successful, false otherwise.
	 */
	public boolean createBetForUser(VizonUser user, VizonDrawing drawing, Bet bet)
	{
		if (user == null || drawing == null || bet == null)
		{
			return false;
		}

		try
		{
			PreparedStatement statement = vizonSql.prepare("INSERT INTO vizon_bets "
					+ "(vizon_users_id, vizon_drawings_id, first, second, third, fourth, fifth, sixth) "
					+ "VALUES(?, ?, ?, ?, ?, ?, ?, ?)");

			statement.setInt(1, user.getId());
			statement.setInt(2, drawing.getId());
			statement.setInt(3, bet.getFirst());
			statement.setInt(4, bet.getSecond());
			statement.setInt(5, bet.getThird());
			statement.setInt(6, bet.getFourth());
			statement.setInt(7, bet.getFifth());
			statement.setInt(8, bet.getSixth());

			int inserted = vizonSql.executeUpdateBlocking(statement);

			return inserted > 0;
		}
		catch (SQLException ex)
		{
			logger.warn("Error while inserting bet for user in Vizon Database", ex);
			return false;
		}
	}

	/**
	 * Finds a scheduled drawing that has not been run yet (i.e. First == null)
	 *
	 * @return {@link VizonDrawing} or null if no unrun drawings exist
	 */
	public VizonDrawing findEarliestUnrunDrawing()
	{
		try
		{
			PreparedStatement statement = vizonSql.prepare("SELECT * FROM vizon_drawings "
					+ "WHERE first IS NULL "
					+ "ORDER BY drawing_date "
					+ "LIMIT 1");

			ResultSet rs = vizonSql.executeQuery(statement);

			if (!rs.next())
			{
				return null;
			}

			return VizonDrawing.fromResultSet(rs);
		}
		catch (SQLException ex)
		{
			logger.warn("Error while trying to find earliest drawing in Vizon Database", ex);
			return null;
		}
	}

	public VizonDrawing createDrawing(LocalDateTime date)
	{
		try
		{
			PreparedStatement statement = vizonSql.prepare("INSERT INTO vizon_drawings "
					+ "(drawing_date) "
					+ "VALUES(?)",
					Statement.RETURN_GENERATED_KEYS);

			statement.setTimestamp(1, Timestamp.valueOf(date));

			int inserted = vizonSql.executeUpdateBlocking(statement);

			ResultSet rs = statement.getGeneratedKeys();

			if (rs == null || !rs.next())
			{
				return null;
			}

			int id = rs.getInt(1);

			statement = vizonSql.prepare("SELECT * FROM vizon_drawings "
					+ "WHERE id = ?");

			statement.setInt(1, id);

			rs = vizonSql.executeQuery(statement);

			if (rs == null || !rs.next())
			{
				return null;
			}

			return VizonDrawing.fromResultSet(rs);
		}
		catch (SQLException ex)
		{
			logger.warn("Error while trying to create drawing in Vizon Database", ex);
			return null;
		}
	}

	public List<VizonUser> findAllUsers()
	{
		List<VizonUser> users = new ArrayList<>();

		try
		{
			PreparedStatement statement = vizonSql.prepare("SELECT * FROM vizon_users");

			ResultSet rs = vizonSql.executeQuery(statement);

			while (rs.next())
			{
				VizonUser user = VizonUser.fromResultSet(rs);

				if (user == null)
				{
					continue;
				}

				users.add(user);
			}
		}
		catch (SQLException ex)
		{
			logger.warn("Error while trying to get all users in Vizon Database", ex);
		}

		return users;
	}

	public int expireVhosts()
	{
		try
		{
			PreparedStatement statement = vizonSql.prepare("UPDATE vizon_users "
					+ "SET vhost = null, "
					+ "eligible = 0, "
					+ "obtained = null, "
					+ "obtained_id = -1, "
					+ "multiplier = 1, "
					+ "permanent = 0, "
					+ "days = 0, "
					+ "bold = 0 "
					+ "WHERE permanent = 0 "
					+ "AND DATE_ADD(obtained, INTERVAL days DAY) < NOW()");

			return vizonSql.executeUpdateBlocking(statement);

		}
		catch (SQLException ex)
		{
			logger.warn("Error in SQL statement to expire vhosts", ex);
			return 0;
		}
	}
}
