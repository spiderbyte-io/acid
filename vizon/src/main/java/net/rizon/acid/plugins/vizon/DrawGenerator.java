/*
 * Copyright (c) 2017, orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.plugins.vizon;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;
import static net.rizon.acid.plugins.vizon.BetValidator.BET_MAX;
import static net.rizon.acid.plugins.vizon.BetValidator.BET_MIN;

/**
 *
 * @author orillion <orillion@rizon.net>
 */
public class DrawGenerator
{
	private final List<Integer> numbers = new ArrayList<>(BET_MAX - BET_MIN + 1);
	private final SecureRandom random = new SecureRandom();

	public DrawGenerator()
	{
		IntStream
			.rangeClosed(BET_MIN, BET_MAX)
			.forEach(i -> numbers.add(i));
	}

	/**
	 * Generates a new drawing with random numbers.
	 *
	 * @return
	 */
	public Bet generate()
	{
		Collections.shuffle(numbers, random);

		Bet bet = new Bet(
			numbers.get(0),
			numbers.get(1),
			numbers.get(2),
			numbers.get(3),
			numbers.get(4),
			numbers.get(5));

		return bet;
	}

	/**
	 * Takes a random bet and declares it the winner. If there are no bets,
	 * it will generate a random one.
	 *
	 * @param bets Bets that were placed
	 *
	 * @return A random bet.
	 */
	public Bet generateSpecial(List<Bet> bets)
	{
		if (bets == null || bets.isEmpty())
		{
			return generate();
		}

		Collections.shuffle(bets, random);

		return bets.get(0);
	}
}
