/*
 * Copyright (c) 2017, orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.plugins.vizon.commands;

import java.time.LocalDateTime;
import java.util.regex.Matcher;
import net.rizon.acid.core.AcidUser;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Command;
import net.rizon.acid.core.User;
import net.rizon.acid.plugins.vizon.RequestStatus;
import net.rizon.acid.plugins.vizon.VhostManager;
import net.rizon.acid.plugins.vizon.Vizon;
import net.rizon.acid.plugins.vizon.db.VizonRequest;
import net.rizon.acid.plugins.vizon.db.VizonUser;

/**
 *
 * @author orillion <orillion@rizon.net>
 */
public class RequestCommand extends Command
{
	private static final int MAX_HOST_BYTES = 63;

	public RequestCommand()
	{
		super(1, 1);
	}

	@Override
	public void Run(User source, AcidUser to, Channel c, String[] args)
	{
		String vhost = args[0] + '\u000F';

		if (!source.isIdentified())
		{
			// User has not identified to NickServ.
			Acidictive.reply(source, to, c, "You need to register or identify to your nickname before you can use this command");
			return;
		}

		if (c != null)
		{
			// Cannot be a channel command
			Acidictive.reply(source, to, null, "This command cannot be used in a channel");
			return;
		}

		VizonUser user = Vizon.getVizonDatabase().findOrCreateUser(source.getNick());
		
		if (!user.isEligible())
		{
			// User not eligible to select a new vhost
			Acidictive.reply(source, to, null, "You are not allowed to select a new vhost");
			return;
		}

		Matcher matcher;

		if (user.isBold())
		{
			matcher = VhostManager.BOLD_PATTERN.matcher(vhost);
		}
		else
		{
			matcher = VhostManager.NORMAL_PATTERN.matcher(vhost);
		}

		if (!matcher.matches())
		{
			// Vhost contains illegal characters
			Acidictive.reply(source, to, null, "Requested vhost contains illegal characters, or is empty");
			return;
		}

		if (vhost.getBytes().length > MAX_HOST_BYTES)
		{
			// We count bytes instead of chars, since counting chars is wrong.
			Acidictive.reply(source, to, null, "Requested vhost is too long");
			return;
		}
		
		VizonRequest request = Vizon.getVizonDatabase().findVhostRequestByUserId(user.getId());

		if (request == null)
		{
			// id doesn't really matter, it will get assigned in the database automatically.
			request = new VizonRequest(
					-1,
					user.getId(),
					user.getNick(),
					vhost,
					RequestStatus.PENDING,
					null,
					null,
					LocalDateTime.now());

			if (!Vizon.getVizonDatabase().insertRequest(request))
			{
				// User not eligible to select a new vhost
				Acidictive.reply(source, to, null, "Something went wrong while registering your vhost request, please try again or contact an operator");
				return;
			}

			request = Vizon.getVizonDatabase().findVhostRequestByUserId(request.getUserId());

			Acidictive.reply(source, to, null, "Vhost requested");

			if (request != null)
			{
				// Should never be null, but you know.
				Vizon.getRequestTracker().addRequest(request);
			}
		}
		else if (request.getVhost().equals(vhost))
		{
			Acidictive.reply(source, to, null, "You already requested this vhost");
		}
		else
		{
			request.setVhost(vhost);

			if (!Vizon.getVizonDatabase().updateRequest(request))
			{
				Acidictive.reply(source, to, null, "Something went wrong while updating your vhost request, please try again or contact an operator");
				return;
			}

			Acidictive.reply(source, to, null, "Request updated");

			Vizon.getRequestTracker().addRequest(request);
		}
	}
}
