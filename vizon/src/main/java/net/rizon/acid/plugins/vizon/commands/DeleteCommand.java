/*
 * Copyright (c) 2017, orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.plugins.vizon.commands;

import net.rizon.acid.core.AcidUser;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Command;
import net.rizon.acid.core.Protocol;
import net.rizon.acid.core.User;
import net.rizon.acid.plugins.vizon.Vizon;
import net.rizon.acid.plugins.vizon.db.VizonUser;

/**
 *
 * @author orillion <orillion@rizon.net>
 */
public class DeleteCommand extends Command
{
	public DeleteCommand()
	{
		super(1, 1);
	}

	@Override
	public void Run(User source, AcidUser to, Channel c, String[] args)
	{
		String nick = args[0];

		VizonUser user = Vizon.getVizonDatabase().findUser(nick);

		if (user == null)
		{
			Acidictive.reply(source, to, c, "Nickname not found in database");
			return;
		}

		user.setObtained(null);
		user.setEligible(false);
		user.setVhost(null);
		user.setBold(false);
		user.setPermanent(false);
		user.setJackpot(false);
		user.resetMultiplier();
		user.resetDays();

		if (Vizon.getVizonDatabase().updateUser(user))
		{
			Acidictive.reply(source, to, c, String.format("Removed colored vhost for user %s", nick));
		}
		else
		{
			Acidictive.reply(source, to, c, "Unable to update user, contact dev team");
		}

		User ircuser = User.findUser(nick);

		if (ircuser == null)
		{
			return;
		}

		Protocol.chghost(ircuser, ircuser.getCloakedHost());
		ircuser.setVhost(ircuser.getCloakedHost());
	}

}
