BeautifulSoup>=3.2.1
mysql-python>=1.2.5
pyparsing>=2.0.2
wordnik>=2.1.2
requests>=0.10.6
PyYAML>=3.11
dnspython>=1.14.0
lxml>=3.6.4
