import pseudoclient.sys_base

from datetime import datetime
from utils import *

from pseudoclient import cmd_admin
from pseudoclient.cmd_admin import \
	admin_unregistered, \
	admin_chan, \
	admin_log, \
	admin_msg, \
	admin_opt, \
	admin_db

def admin_sys(self, source, target, pieces):
	if len(pieces) < 2:
		return False

	starget = pieces[0]
	operation = pieces[1]
	names = []
	subsystems = []

	if 'o' in starget:
		names.append('options')
		subsystems.append(self.options)

	if 'c' in starget:
		names.append('channels')
		subsystems.append(self.channels)

	if 'a' in starget:
		names.append('auth')
		subsystems.append(self.auth)

	if len(names) == 0:
		return False

	if operation in ['u', 'update']:
		for subsystem in subsystems:
			subsystem.force()

		self.msg(target, 'Forced update for @b%s@b.' % '@b, @b'.join(names))
	elif operation in ['r', 'reload']:
		for subsystem in subsystems:
			subsystem.reload()

		self.msg(target, 'Forced reload for @b%s@b.' % '@b, @b'.join(names))
	elif operation in ['d', 'delay']:
		if len(pieces) == 2:
			for subsystem in subsystems:
				self.msg(target, 'Auto-update delay for @b%s@b is %d seconds.' % (subsystem.name, subsystem.delay))
		else:
			try:
				seconds = int(pieces[2])
			except:
				return False

			if seconds < 10:
				self.msg(target, 'Auto-update delay must be greater than 10 seconds.')
				return True

			for subsystem in subsystems:
				subsystem.set_option('update_period', seconds)
				subsystem.reload()

			self.msg(target, 'Auto-update delay for @b%s@b set to @b%d@b seconds.' % ('@b, @b'.join(names), seconds))
	else:
		return False

	return True

def admin_stats(self, source, target, pieces):
	self.msg(target, 'Registered channels: @b%d@b.' % len(self.channels.list_all()))
	return True

def admin_qsize(self, source, target, pieces):
	self.msg(target, 'Queue size: %d' % len(self.limit_monitor))
	return True

def admin_delay(self, source, target, pieces):
	if not pieces:
		self.msg(target, 'Delay: %d seconds' % self.limit_monitor._delay)
		return True
	
	try:
		new_delay = int(pieces[0])
	except ValueError:
		return False
	
	self.limit_monitor.set_delay(new_delay)
	self.msg(target, 'New delay set to %d seconds' % new_delay)
	return True

def get_commands():
	return {
		'chan'       : (admin_chan,            '<ban|unban|info|add|remove|list|blist> <channel> [reason]'),
		'unreg'      : (admin_unregistered,    '<check|list|part> - remove unregistered channels'),
		'stats'      : (admin_stats,           'counts registered channels'),
		'db'         : (admin_db,              '[on|off] - enables/disables auto commits to db'),
		'opt'        : (admin_opt,             '[get|set|clear] [option] [value] - manipulates options (list all if no arguments)'),
		'sys'        : (admin_sys,             '<subsystem> <operation> [value] - (subsystems: options (o), channels (c), auth (a)) (operations: update (u), reload (r), delay (d))'),
		'log'        : (admin_log,             '[level] - gets or sets the log level (0-7).'),
		'msg'        : (admin_msg,             '<message> - sends a message to all channels'),
		'qsize'      : (admin_qsize,           '[debug command] shows channel update queue size'),
		'delay'      : (admin_delay,           '<new_delay> - (seconds) changes the interval between limit changes')
	}
