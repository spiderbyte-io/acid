#!/usr/bin/python pseudoserver.py
# psm_moo.py
# module for pypseudoserver
# written by ElChE <elche@rizon.net>, martin <martin@rizon.net>
#
# vHost module

import traceback
import logging
import sys
import re

from utils import format_ascii_irc
from pseudoclient import sys_log
from datetime import datetime

from pyva import *
from core import *
from plugin import *

import cmd_user, requests

import pyva_net_rizon_acid_core_Acidictive as Acidictive
import pyva_net_rizon_acid_core_User as User

class moo(AcidPlugin):
	# Regexes for HostServ messages because 1.8/2.0 duality.
	req_regexes = {'req18': re.compile('^New vHost (.+?) requested by (.+?) \((.+)\)$'),
			'req19': re.compile('^COMMAND: (.+?)!.+?@.+? (.+?) used REQUEST to request new vhost (.+)$'),
			'rejacc18': re.compile('^Host Request for (.+?) (.+?) by (.+)$'),
			'acc19': re.compile('^COMMAND: (.+?)!.+?@.+? .+? used ACTIVATE for (.+?) for vhost .+$'),
			'rej19': re.compile('^COMMAND: (.+?)!.+?@.+? .+? used REJECT to reject vhost for (.+?) \(.*\)$'),
			'wait18': re.compile("^#\d+ Nick:\002(.+?)\002, vhost:\002(.+?)\002 \(.+\)$"),
			'wait19': re.compile('^\d+ +([^ ]+) +([^ ]+) +([^ ]+) +.+$')
			}

	
	def __init__(self):
		AcidPlugin.__init__(self)

		self.name = "moo"
		self.log = logging.getLogger(__name__)

		try:
			self.client = istring(self.config.get('vhost').get('nick'))
			self.chan = istring(self.config.get('vhost').get('channel'))
			self.logchan = istring(self.config.get('vhost').get('logchan'))
		except Exception, err:
			self.log.exception("Error reading vhost configuration options: %s" % err)
			raise
		
	def start(self):
		try:
			self.elog = sys_log.LogManager(self)
			self.commands_user = cmd_user.UserCommandManager()
			self.requests = requests.RequestManager(self)
		except Exception, err:
			self.log.exception('Error initializing core subsystems for vhost module (%s)' % err)
			raise
		
		self.elog.debug('Started core subsystems.')		
		self.initialized = True
		
		if Acidictive.me and not Acidictive.me.isBursting():
			self.msg('HostServ', 'WAITING') # Check if we missed requests while down
		
		return True
	
	def msg(self, target, message):
		if message != '':
			Acidictive.privmsg(self.client, target, format_ascii_irc(message))
	
	def notice(self, target, message):
		if message != '':
			Acidictive.notice(self.client, target, format_ascii_irc(message))
	
	def execute(self, manager, command, argument, channel, sender):
		full_command = '%s%s' % (command, ' %s' % argument if len(argument) else '')
		cmd = manager.get_command(command)
		
		if not cmd:
			return
		
		try:
			cmd[0](self, manager, channel, sender, argument)
		except Exception, e:
			tb = traceback.extract_tb(sys.exc_info()[2])
			longest = 0

			for entry in tb:
				length = len(entry[2])

				if length > longest:
					longest = length

			self.elog.exception(e)
			self.log.exception("moo error!")

			for entry in tb:
				self.elog.traceback('@b%-*s@b : %d %s' % (longest, entry[2], entry[1], entry[3]))
		

	def onSync(self):
		self.msg('HostServ', 'WAITING') # Check if we missed requests while down

	def onNotice(self, source, target, message):
		msg = message.strip()

		if msg.startswith('[#'): #It's a channel welcome message. Let's ignore it.
			return
		
		user = User.findUser(target)
		userinfo = User.findUser(source)

		if not user or user.getNick() != self.client:
			return

		if userinfo.getNick() != 'HostServ':
			return
		
		# Check for 1.8 or 1.9 requests.
		# 1.8:waiting: #$num Nick:\002$nick\002, vhost:\002$vhost\002 ($display - $date)
		# 1.9:waiting: $num     $nick   $display  $vhost  $date
		sp = msg.split(' ')
		
		# Regex. Because it solves the problem in the easiest way.
		match = self.req_regexes['wait18'].match(msg)
		if match == None:
			match = self.req_regexes['wait19'].match(msg)
			if match == None or match.groups(0) == 'Number':
				return

		if len(match.groups()) == 3:
			(nick, vhost, display) = match.groups()
		elif len(match.groups()) == 2:
			(nick, vhost) = match.groups()
			display = None # self.requests.add can handle None for display nick
		else:
			return

		if not nick or not vhost:
			return

		try:
			self.requests.add(vhost, nick, display)
		except Exception, e:
			tb = traceback.extract_tb(sys.exc_info()[2])
			longest = 0
	
			for entry in tb:
				length = len(entry[2])
	
				if length > longest:
					longest = length
	
			self.elog.exception(e)
			self.log.exception("moo error!")
	
			for entry in tb:
				self.elog.traceback('@b%-*s@b : %d %s' % (longest, entry[2], entry[1], entry[3]))

	def onPrivmsg(self, source, target, message):
		userinfo = User.findUser(source)

		sender = userinfo.getNick()
		channel = target
		msg = message.strip()
		index = msg.find(' ')

		if index == -1:
			command = msg
			argument = ''
		else:
			command = msg[:index]
			argument = msg[index + 1:]

		if channel == self.client: # XXX uid
			pass #private message
		elif channel == self.chan and command.startswith('!'):
			self.execute(self.commands_user, command, argument, channel, sender)
		elif channel == self.logchan and (sender == 'Global' or sender == 'HostServ'):
			match = self.req_regexes['req18'].match(msg)
			if match != None:
				try:
					self.requests.add(match.group(1), match.group(2), match.group(3))
				except Exception, e:
					self.print_traceback(e)
				return

			match = self.req_regexes['req19'].match(msg)
			if match != None:
				try:
					self.requests.add(match.group(3), match.group(1), match.group(2))
				except Exception, e:
					self.print_traceback(e)
				return

			match = self.req_regexes['rejacc18'].match(msg)
			if match != None:
				(nick, action, by) = match.groups()
				by = by.split(' ')[0] # In case there's a reject reason
				if by == self.client:
					return

				try:
					if action == 'rejected':
						self.requests.delete(nick)
						self.msg(self.chan, 'vHost for @b%s@b was manually %s by @b%s@b' % (nick, action, by))
					elif action == 'activated':
						self.requests.approve(nick, by, silent=True)
						self.msg(self.chan, 'vHost for @b%s@b was manually %s by @b%s@b' % (nick, action, by))
				except Exception, e:
					self.print_traceback(e)
				return

			match = self.req_regexes['acc19'].match(msg)
			if match != None:
				(by, nick) = match.groups()
				if by == self.client:
					return

				try:
					self.requests.delete(nick)
					self.msg(self.chan, 'vHost for @b%s@b was manually activated by @b%s@b' % (nick, by))
				except Exception, e:
					self.print_traceback(e)
				return

			match = self.req_regexes['rej19'].match(msg)
			if match != None:
				(by, nick) = match.groups()
				if by == self.client:
					return

				try:
					self.requests.delete(nick)
					self.msg(self.chan, 'vHost for @b%s@b was manually rejected by @b%s@b' % (nick, by))
				except Exception, e:
					self.print_traceback(e)
				return

	def print_traceback(self, e):
		tb = traceback.extract_tb(sys.exc_info()[2])
		longest = 0

		for entry in tb:
			length = len(entry[2])

			if length > longest:
				longest = length

		self.elog.exception(e)
		self.log.exception("moo error!")

		for entry in tb:
			self.elog.traceback('@b%-*s@b : %d %s' % (longest, entry[2], entry[1], entry[3]))
