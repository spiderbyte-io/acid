import feed
import utils
from feed import XmlFeed

def from_id(id):
	return Company(XmlFeed('http://api.erepublik.com/v2/feeds/companies/%d' % int(id)))

def from_dict(dict):
	return from_id(dict['id'])

class Company:
	"""A company of eRepublik"""

	def __init__(self, f):
		self.id = f.int('/company/id')
		self.name = f.text('/company/name')
		self.industry = utils.get_industry(f.text('/company/industry/name'))
		self.country = {
			'name': f.text('/company/country/name'),
			'id': f.int('/company/country/id'),
			'code': f.text('/company/country/code')}
		self.quality = f.int('/company/customization-level')
		self.is_for_sale = 'true' in f.text('/company/is-for-sale')

		self.job_offers = [{
			'country': job.text('country'), 'amount': job.int('number-of-jobs'),
			'skill': job.int('required-skill')}
			for job in f.elements('/company/job-offers/job-offer')]

		self.employees = [{
			'country': {'name': emp.text('country/name'), 'id': emp.int('country/id'), 'code': emp.text('country/code')},
			'id': emp.int('id'), 'name': emp.text('name')}
			for emp in f.elements('/company/employees/employee')]

