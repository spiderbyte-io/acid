import feed
import utils
from feed import get_json, HtmlFeed
from datetime import datetime
from decimal import Decimal
from urllib import quote

def from_name(name, secura):
	newname=str(name).lower()
	if isinstance(name, unicode):
		newname = name.encode('utf-8').lower()
	# Another "easy" way around. To be changed.
	
	server = 'secura' if secura=='secura' else 'suna' if secura=='suna' else 'europa' if secura=='europa' else 'primera'
	return Citizen(feed.get_json('http://%s.e-sim.org/apiCitizenByName.html?name=%s' % (server, quote(newname))))

def from_id(id, secura):
	# Another "easy" way around. To be changed.
	server = 'secura' if secura=='secura' else 'suna' if secura=='suna' else 'europa' if secura=='europa' else 'primera'
	return Citizen(feed.get_json('http://%s.e-sim.org/apiCitizenById.html?id=%d' % (server, int(id))))

class Citizen:
	def __init__(self, data):
		if 'error' in data:
			raise feed.FeedError(data['error'])
		
		self.id = data['id']
		self.name = data['login']
		self.exp = data['xp']
		self.level = data['level']
		self.strength = data['strength']
		self.rank = data['rank']
		self.economySkill = data['economySkill']
		self.economySkillboost = data['eqIncreaseEcoSkill']
		self.dmgToday = data['damageToday']
		self.dmgTotal = data['totalDamage']
		self.citizenship = data['citizenship']
		self.region = utils.get_region_by_id(data['currentLocationRegionId'])
		self.premiumDays = data['premiumDays']
		self.activeStatus = data['status']
		self.is_organization = data['organization']
