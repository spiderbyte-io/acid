from pseudoclient.sys_auth import AuthManager

class InternetsAuthManager(AuthManager):
	def __init__(self, module):
		AuthManager.__init__(self, module)

	def onAccept(self, user, request, action, channel):
		if action == 'ytinfo':
			if self.module.channels.is_valid(channel):
				self.module.channels.set(channel, 'youtube_info', True)
				self.module.msg(user, 'Enabled YouTube URL information lookup in @b%s@b.' % channel)
		elif action == 'noytinfo':
			if self.module.channels.is_valid(channel):
				self.module.channels.set(channel, 'youtube_info', False)
				self.module.msg(user, 'Disabled YouTube URL information lookup in @b%s@b.' % channel)
		else:
			return False

		return True
