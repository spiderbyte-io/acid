from datetime import datetime
from utils import *

#---------------------------------------------------------------------#

from pseudoclient import cmd_admin
from pseudoclient.cmd_admin import \
	admin_unregistered, \
	admin_chan, \
	admin_log, \
	admin_msg, \
	admin_opt, \
	admin_db

#---------------------------------------------------------------------#

def admin_user(self, source, target, pieces):
	return cmd_admin.admin_user(self, source, target, pieces, meta={
		'extra_info': lambda user: ' Location: @b%s@b.' % user.location if user.location else ''
	}) 

#---------------------------------------------------------------------#

def admin_sys(self, source, target, pieces):
	if len(pieces) < 2:
		return False

	starget = pieces[0]
	operation = pieces[1]
	names = []
	subsystems = []

	if 'o' in starget:
		names.append('options')
		subsystems.append(self.options)

	if 'u' in starget:
		names.append('users')
		subsystems.append(self.users)

	if 'c' in starget:
		names.append('channels')
		subsystems.append(self.channels)

	if 'n' in starget:
		names.append('news')
		subsystems.append(self.news)

	if 'a' in starget:
		names.append('auth')
		subsystems.append(self.auth)

	if 'f' in starget:
		names.append('antiflood')
		subsystems.append(self.antiflood)

	if len(names) == 0:
		return False

	if operation in ['u', 'update']:
		for subsystem in subsystems:
			subsystem.force()

		self.msg(target, 'Forced update for @b%s@b.' % '@b, @b'.join(names))
	elif operation in ['r', 'reload']:
		for subsystem in subsystems:
			subsystem.reload()

		self.msg(target, 'Forced reload for @b%s@b.' % '@b, @b'.join(names))
	elif operation in ['d', 'delay']:
		if len(pieces) == 2:
			for subsystem in subsystems:
				self.msg(target, 'Auto-update delay for @b%s@b is %d seconds.' % (subsystem.name, subsystem.delay))
		else:
			try:
				seconds = int(pieces[2])
			except:
				return False

			if seconds < 10:
				self.msg(target, 'Auto-update delay must be greater than 10 seconds.')
				return True

			for subsystem in subsystems:
				subsystem.set_option('update_period', seconds)
				subsystem.reload()

			self.msg(target, 'Auto-update delay for @b%s@b set to @b%d@b seconds.' % ('@b, @b'.join(names), seconds))
	else:
		return False

	return True


def admin_stats(self, source, target, pieces):
	self.msg(target, 'Registered users: @b%d@b.' % len(self.users.list_all()))
	self.msg(target, 'Registered channels: @b%d@b.' % len(self.channels.list_all()))
	return True

def get_commands():
	return {
		'chan'       : (admin_chan,            '<ban|unban|info|add|remove|list|blist|news> <channel> [reason]'),
		'unreg'      : (admin_unregistered,    '<check|list|part> - remove unregistered channels'),
		'user'       : (admin_user,            '<ban|unban|info|add|remove|list|blist> <user> [reason]'),
		'stats'      : (admin_stats,           'counts registered users and channels'),
		'db'         : (admin_db,              '[on|off] - enables/disables auto commits to db'),
		'opt'        : (admin_opt,             '[get|set|clear] [option] [value] - manipulates options (list all if no arguments)'),
		'sys'        : (admin_sys,             '<subsystem> <operation> [value] - (subsystems: options (o), users (u), channels (c), news (n), auth (a), antiflood (f)) (operations: update (u), reload (r), delay (d))'),
		'log'        : (admin_log,             '[level] - gets or sets the log level (0-7).'),
		'msg'        : (admin_msg,             '<message> - sends a message to all channels'),
	}

