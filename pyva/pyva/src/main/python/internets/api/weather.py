# NOTE: You need to have the ZIP code database imported in order for Weather to perform ZIP code lookups

# Code note: In get_conditions/get_forecast, zipcode is used primarily for caching
# While it would not strictly be necessary for the weather/etc querying to work, this code
#  assumes it is there if a lat/long is given as the 'loctation' input

import datetime as dt
from feed import get_json, FeedError


def c_to_f(temp):
	"""Convert given celcius temperature to farenheit."""
	return ((9.0 / 5.0) * temp) + 32


class Weather(object):
	def __init__(self, key):
		self.API_KEY = key
		self.city_name_cache = {}  # cache of names -> city id
		self.condition_cache = {}  # cache of conditions, lasts 10 minutes
		self.forecast_cache = {}  # cache of forecasts, lasts 30 minutes
		self.last_min_requests = []
		self.REQ_LIMIT = 99

	def api_request(self, url):
		"""Perform an API request, including respecting sane request limits"""
		self.last_min_requests = [req for req in self.last_min_requests if (dt.datetime.now() - req).seconds < 60]

		if len(self.last_min_requests) >= self.REQ_LIMIT:
			raise WeatherException('weather data is temporarily unavailable. Try again later')

		self.last_min_requests.append(dt.datetime.now())
		try:
			data = get_json(url)
			if data['cod'] == '404':
				raise WeatherException('No cities match your search query')
			return data
		except FeedError, e:
			if getattr(e, 'code', 0) == 512:  # openweathermap returns this for no city, apparently. well, used to, that is
				raise WeatherException('No cities match your search query')
			else:
				raise e

	def get_conditions(self, location):
		"""Return weather conditions for given location, using OpenWeatherMap.
		If location is string, evaluate it as city name.
		If location is list, evaluate as [latitude, longitude].
		"""

		# id caching
		location_id = None

		if isinstance(location, str):
			if location.lower() in self.city_name_cache:
				location_id = self.city_name_cache[location.lower()]

		# check forecast cache, if we now have a valid ID
		if location_id is not None:
			# if we have it in our cache, and it's not expired, use that instead
			if location_id in self.condition_cache:
				weather_data = self.condition_cache.get(location_id, {'ts': 1})
				if ((dt.datetime.now() - weather_data['ts']).seconds / 60) < 10:  # 10 minutes
					return weather_data['conditions']

		# we know that 'xxx' can't be a valid OpenWeatherMap key
		#  and we wanna be nice to these guys, free and open and all
		if self.API_KEY == 'xxx':
			raise WeatherException('this key is not valid')

		if location_id is not None:
			api_data = self.api_request('http://api.openweathermap.org/data/2.5/weather?id={location_id}&APPID={key}&units=metric'.format(key=self.API_KEY, location_id=location_id))
		elif isinstance(location, str):
			api_data = self.api_request('http://api.openweathermap.org/data/2.5/weather?q={location}&APPID={key}&units=metric'.format(key=self.API_KEY, location=location))
		# elif type(location) == list or type(location) == tuple:
		# 	api_data = self.api_request('http://api.openweathermap.org/data/2.5/weather?lat={location[0]}&lon={location[1]}&APPID={key}&units=metric'.format(key=self.API_KEY, location=location))
		else:
			raise Exception('weather: location type {} not supported'.format(type(location)))

		# set our location cache for next time
		if isinstance(location, str):
			self.city_name_cache[location.lower()] = api_data['id']

		# silly hack
		api_data['weather'][0]['description'] = api_data['weather'][0]['description'].title()

		condition_data = {
			'id': api_data['id'],
			'city': api_data['name'] + u', ',
			'country': api_data['sys']['country'],
			'description': api_data['weather'][0]['description'].title().replace(' Is ', ' is '),
			'temp_c': api_data['main']['temp'],
			'temp_f': c_to_f(api_data['main']['temp']),  # convert it manually
			'pressure': api_data['main']['pressure'],
			'humidity': api_data['main']['humidity'],
			'rain': 'No Data Available',
		}

		# make the country actually consistent
		if condition_data['country'] in ['United States of America', 'US']:
			condition_data['country'] = 'USA'

		if not api_data['name']:  # this is a country, not a specific city
			condition_data['city'] = ''

		if 'rain' in api_data:  # can be included or not, eg Japan
			if api_data['rain']:
				rain_time = api_data['rain'].keys()[0]  # since rain time can be '1h', '3h', etc
				condition_data['rain'] = '{}mm/{}'.format(api_data['rain'][rain_time], rain_time)
			else:
				condition_data['rain'] = 'Not Raining'

		# and set our data, with appropriate timeout
		self.condition_cache[api_data['id']] = {
			'ts': dt.datetime.now(),
			'conditions': condition_data,
		}

		return condition_data

	def get_forecast(self, location):
		"""Return weather forecast for given location, using OpenWeatherMap.
		If location is string, evaluate it as city name.
		If location is list, evaluate as [latitude, longitude].
		"""

		# id caching
		location_id = None

		if isinstance(location, str):
			if location.lower() in self.city_name_cache:
				location_id = self.city_name_cache[location.lower()]

		# check forecast cache, if we now have a valid ID
		if location_id is not None:
			# if we have it in our cache, and it's not expired, use that instead
			if location_id in self.forecast_cache:
				weather_data = self.forecast_cache.get(location_id, {'ts': 1})
				if ((dt.datetime.now() - weather_data['ts']).seconds / 60) < 10:  # 10 minutes
					return weather_data['forecast']

		# we know that 'xxx' can't be a valid OpenWeatherMap key
		#  and we wanna be nice to these guys, free and open and all
		if self.API_KEY == 'xxx':
			raise WeatherException('this key is not valid')

		if location_id is not None:
			api_data = self.api_request('http://api.openweathermap.org/data/2.5/forecast?id={location_id}&APPID={key}&units=metric'.format(key=self.API_KEY, location_id=location_id))
		elif isinstance(location, str):
			api_data = self.api_request('http://api.openweathermap.org/data/2.5/forecast?q={location}&APPID={key}&units=metric'.format(key=self.API_KEY, location=location))
		else:
			raise Exception('get_conditions: location type {} not supported'.format(type(location)))

		# set our location cache for next time
		if isinstance(location, str):
			self.city_name_cache[location.lower()] = api_data['city']['id']

		forecast_data = {
			'id': api_data['city']['id'],
			'city': api_data['city']['name'] + u', ',
			'country': api_data['city']['country'],
			'days': [],
		}

		# make the country actually consistent
		if forecast_data['country'] in ['United States of America', 'US']:
			forecast_data['country'] = 'USA'

		if not api_data['city']['name']:  # this is a country, not a specific city
			forecast_data['city'] = ''

		# assemble and insert specific day data
		today = dt.datetime.now()
		i = 0

		for day in api_data['list'][:4]:
			day_data = {
				'name': (today + dt.timedelta(days=i)).strftime('%A'),
				'description': day['weather'][0]['description'].title().replace(' Is ', ' is '),
				'min_c': int(day['main']['temp_min']),
				'min_f': int(c_to_f(day['main']['temp_min'])),
				'max_c': int(day['main']['temp_max']),
				'max_f': int(c_to_f(day['main']['temp_max'])),
			}

			forecast_data['days'].append(day_data)
			i += 1

		# and set our data, with appropriate timeout
		self.forecast_cache[api_data['city']['id']] = {
			'ts': dt.datetime.now(),
			'forecast': forecast_data,
		}

		return forecast_data


class WeatherException(Exception):
	def __init__(self, msg):
		self.msg = msg

	def __str__(self):
		return str(self.msg)
