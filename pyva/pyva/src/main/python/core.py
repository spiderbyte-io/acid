import yaml
from istring import istring
import logging
import logging.handlers
import pool

with open('pypsd.yml', 'r') as config_fd:
	config = yaml.load(config_fd)

anope_major = int(config.get('services').get('anope_major'))
if anope_major not in [1, 2]:
	raise Exception('Unknown anope major version %s' % anope_major) 

dbpool = pool.DBPool(config)
dbx = dbpool.get_connection()

logfile = config.get('logging').get('logfile')
loglevel = getattr(logging, config.get('logging').get('level').upper())
	
FORMAT = '%(asctime)s %(name)s(%(lineno)s) [%(levelname)s] %(message)s'
handler = logging.handlers.TimedRotatingFileHandler(
	logfile, when='midnight', backupCount=7)
handler.setFormatter(logging.Formatter(FORMAT))
	
log = logging.getLogger('')
log.addHandler(handler)
log.setLevel(loglevel)

import pyva_java_lang_System as javasys
import sys

class StdOutWriter(object):
	def write(self, what):
		# print is a Python keyword
		javasys.out['print'](what)

class StdErrWriter(object):
	def write(self, what):
		javasys.err['print'](what)

sys.stdout = StdOutWriter()
sys.stderr = StdErrWriter()
