from collection import *
from sys_base import *
import inspect

class User(CollectionEntity):
	def __init__(self, id, name):
		CollectionEntity.__init__(self, id)
		self.name = name

class UserManager(CollectionManager, Subsystem):
	def __init__(self, module, type = User):
		Subsystem.__init__(self, module, module.options, 'users')
		CollectionManager.__init__(self, type)
